﻿namespace Nesne_Lab_Project
{
    partial class User_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(User_Info));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_kadi = new System.Windows.Forms.TextBox();
            this.txt_statu = new System.Windows.Forms.TextBox();
            this.txt_pass = new System.Windows.Forms.TextBox();
            this.lbl_pass = new System.Windows.Forms.Label();
            this.cmbox_gender = new System.Windows.Forms.ComboBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.pctbx_photo = new System.Windows.Forms.PictureBox();
            this.imglst_userPhoto = new System.Windows.Forms.ImageList(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txt_mail = new System.Windows.Forms.TextBox();
            this.btn_editKadi = new System.Windows.Forms.Button();
            this.btn_editMail = new System.Windows.Forms.Button();
            this.btn_editGender = new System.Windows.Forms.Button();
            this.btn_editPass = new System.Windows.Forms.Button();
            this.btn_editPhoto = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_photo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(165, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kullanici adi:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(197, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Statu:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(185, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cinsiyet:";
            // 
            // txt_kadi
            // 
            this.txt_kadi.Location = new System.Drawing.Point(237, 48);
            this.txt_kadi.Name = "txt_kadi";
            this.txt_kadi.ReadOnly = true;
            this.txt_kadi.Size = new System.Drawing.Size(163, 20);
            this.txt_kadi.TabIndex = 0;
            this.txt_kadi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // txt_statu
            // 
            this.txt_statu.Location = new System.Drawing.Point(238, 22);
            this.txt_statu.Name = "txt_statu";
            this.txt_statu.ReadOnly = true;
            this.txt_statu.Size = new System.Drawing.Size(163, 20);
            this.txt_statu.TabIndex = 9;
            // 
            // txt_pass
            // 
            this.txt_pass.Location = new System.Drawing.Point(237, 126);
            this.txt_pass.Name = "txt_pass";
            this.txt_pass.PasswordChar = '*';
            this.txt_pass.ReadOnly = true;
            this.txt_pass.Size = new System.Drawing.Size(163, 20);
            this.txt_pass.TabIndex = 3;
            this.txt_pass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // lbl_pass
            // 
            this.lbl_pass.AutoSize = true;
            this.lbl_pass.Location = new System.Drawing.Point(175, 129);
            this.lbl_pass.Name = "lbl_pass";
            this.lbl_pass.Size = new System.Drawing.Size(56, 13);
            this.lbl_pass.TabIndex = 5;
            this.lbl_pass.Text = "Password:";
            // 
            // cmbox_gender
            // 
            this.cmbox_gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbox_gender.Enabled = false;
            this.cmbox_gender.FormattingEnabled = true;
            this.cmbox_gender.Location = new System.Drawing.Point(237, 100);
            this.cmbox_gender.Name = "cmbox_gender";
            this.cmbox_gender.Size = new System.Drawing.Size(164, 21);
            this.cmbox_gender.TabIndex = 2;
            this.cmbox_gender.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(272, 152);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(72, 23);
            this.btn_save.TabIndex = 7;
            this.btn_save.Text = "Kaydet";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            this.btn_save.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // pctbx_photo
            // 
            this.pctbx_photo.Location = new System.Drawing.Point(12, 12);
            this.pctbx_photo.Name = "pctbx_photo";
            this.pctbx_photo.Size = new System.Drawing.Size(129, 137);
            this.pctbx_photo.TabIndex = 9;
            this.pctbx_photo.TabStop = false;
            // 
            // imglst_userPhoto
            // 
            this.imglst_userPhoto.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imglst_userPhoto.ImageStream")));
            this.imglst_userPhoto.TransparentColor = System.Drawing.Color.Transparent;
            this.imglst_userPhoto.Images.SetKeyName(0, "no-gender.png");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(202, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mail:";
            // 
            // txt_mail
            // 
            this.txt_mail.Location = new System.Drawing.Point(238, 74);
            this.txt_mail.Name = "txt_mail";
            this.txt_mail.ReadOnly = true;
            this.txt_mail.Size = new System.Drawing.Size(163, 20);
            this.txt_mail.TabIndex = 1;
            this.txt_mail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // btn_editKadi
            // 
            this.btn_editKadi.Location = new System.Drawing.Point(407, 46);
            this.btn_editKadi.Name = "btn_editKadi";
            this.btn_editKadi.Size = new System.Drawing.Size(54, 23);
            this.btn_editKadi.TabIndex = 12;
            this.btn_editKadi.Text = "Düzenle";
            this.btn_editKadi.UseVisualStyleBackColor = true;
            this.btn_editKadi.Click += new System.EventHandler(this.btn_edit_Click);
            this.btn_editKadi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // btn_editMail
            // 
            this.btn_editMail.Location = new System.Drawing.Point(407, 72);
            this.btn_editMail.Name = "btn_editMail";
            this.btn_editMail.Size = new System.Drawing.Size(54, 23);
            this.btn_editMail.TabIndex = 12;
            this.btn_editMail.Text = "Düzenle";
            this.btn_editMail.UseVisualStyleBackColor = true;
            this.btn_editMail.Click += new System.EventHandler(this.btn_edit_Click);
            this.btn_editMail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // btn_editGender
            // 
            this.btn_editGender.Location = new System.Drawing.Point(407, 98);
            this.btn_editGender.Name = "btn_editGender";
            this.btn_editGender.Size = new System.Drawing.Size(54, 23);
            this.btn_editGender.TabIndex = 12;
            this.btn_editGender.Text = "Düzenle";
            this.btn_editGender.UseVisualStyleBackColor = true;
            this.btn_editGender.Click += new System.EventHandler(this.btn_edit_Click);
            this.btn_editGender.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // btn_editPass
            // 
            this.btn_editPass.Location = new System.Drawing.Point(407, 124);
            this.btn_editPass.Name = "btn_editPass";
            this.btn_editPass.Size = new System.Drawing.Size(54, 23);
            this.btn_editPass.TabIndex = 12;
            this.btn_editPass.Text = "Düzenle";
            this.btn_editPass.UseVisualStyleBackColor = true;
            this.btn_editPass.Click += new System.EventHandler(this.btn_edit_Click);
            this.btn_editPass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // btn_editPhoto
            // 
            this.btn_editPhoto.Location = new System.Drawing.Point(12, 152);
            this.btn_editPhoto.Name = "btn_editPhoto";
            this.btn_editPhoto.Size = new System.Drawing.Size(56, 23);
            this.btn_editPhoto.TabIndex = 12;
            this.btn_editPhoto.Text = "Düzenle";
            this.btn_editPhoto.UseVisualStyleBackColor = true;
            this.btn_editPhoto.Click += new System.EventHandler(this.btn_edit_Click);
            this.btn_editPhoto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            // 
            // User_Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 191);
            this.Controls.Add(this.btn_editPhoto);
            this.Controls.Add(this.btn_editPass);
            this.Controls.Add(this.btn_editGender);
            this.Controls.Add(this.btn_editMail);
            this.Controls.Add(this.btn_editKadi);
            this.Controls.Add(this.pctbx_photo);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.cmbox_gender);
            this.Controls.Add(this.lbl_pass);
            this.Controls.Add(this.txt_pass);
            this.Controls.Add(this.txt_statu);
            this.Controls.Add(this.txt_mail);
            this.Controls.Add(this.txt_kadi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Name = "User_Info";
            this.Text = "User_Info";
            this.Load += new System.EventHandler(this.User_Info_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.User_Info_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_photo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_kadi;
        private System.Windows.Forms.TextBox txt_statu;
        private System.Windows.Forms.TextBox txt_pass;
        private System.Windows.Forms.Label lbl_pass;
        private System.Windows.Forms.ComboBox cmbox_gender;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.PictureBox pctbx_photo;
        private System.Windows.Forms.ImageList imglst_userPhoto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_mail;
        private System.Windows.Forms.Button btn_editKadi;
        private System.Windows.Forms.Button btn_editMail;
        private System.Windows.Forms.Button btn_editGender;
        private System.Windows.Forms.Button btn_editPass;
        private System.Windows.Forms.Button btn_editPhoto;
    }
}