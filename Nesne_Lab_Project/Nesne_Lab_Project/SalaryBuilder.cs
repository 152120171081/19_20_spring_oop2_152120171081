﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    public abstract class SalaryBuilder
    {
        protected Salary SalaryCalculate;

        public abstract void SetIndexofdeneyim(int index);
        public abstract void SetIndexofyasanilanil(int index);
        public abstract void SetIndexofustogrenim(int index);
        public abstract void SetIndexofyabancidilbilgisi(int index);
        public abstract void SetIndexofyoneticilik(int index);
        public abstract void SetIndexofailedurumu(int index);
        public abstract void SetDeneyim(double value);
        public abstract void SetYasanilanil(double value);
        public abstract void SetUstogrenim(double value);
        public abstract void SetYabancidilbilgisi(double value);
        public abstract void SetYoneticilik(double value);
        public abstract void SetAiledurumu(double value);
        public abstract void SetCheckevli(bool boolValue);
        public abstract void SetCheck06(bool boolValue);
        public abstract void SetCheck718(bool boolValue);
        public abstract void SetCheck18ustu(bool boolValue);
        public abstract void SetCheckBelge(bool boolValue);
        public abstract void SetCheckDiploma(bool boolValue);
        public abstract void SetCheckExtraBelge(bool boolValue);
        public abstract void SetExtraBelgeAdet(int index);
        public abstract void SetIdNumber(int idNumber);
        public abstract void SetMaasBilgisi(double maasBilgisi);
        public void CreateNewSalary()
        {
            SalaryCalculate = new Salary();
        }
        public Salary GetSalary()
        {
            return SalaryCalculate;
        }
    }
}
