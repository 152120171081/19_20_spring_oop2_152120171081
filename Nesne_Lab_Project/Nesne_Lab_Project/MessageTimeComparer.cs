﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    class MessageTimeComparer : IComparer<Message>
    {
        public int Compare(Message x, Message y)
        {
            return x.Time.CompareTo(y.Time);
        }
    }
}
