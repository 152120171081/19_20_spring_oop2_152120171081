﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Nesne_Lab_Project
{
    public static class FileOperatin
    {
        public static void Read(string filepath,List<User> users)
        {
            try
            {
                if (!File.Exists(filepath))
                {
                    StreamWriter sw = new StreamWriter(filepath, true, Encoding.UTF8);
                    sw.Close();
                }
                if (users.Count > 0)
                    users.Clear();
                StreamReader sr = new StreamReader(filepath, Encoding.UTF8, true);
                string line = "";
                while (true)
                {
                    line = sr.ReadLine();
                    if (line == null)
                        break;
                    string[] lineSep = line.Split(',');
                    users.Add(new User(int.Parse(lineSep[0]), lineSep[1], lineSep[2], lineSep[3], lineSep[4], lineSep[5], lineSep[6].Equals("True")));
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void Read(string filepath, List<string> data)
        {
            try
            {
                if (!File.Exists(filepath))
                {
                    StreamWriter sw = new StreamWriter(filepath, true, Encoding.UTF8);
                    sw.Close();
                }
                if (data.Count > 0)
                    data.Clear();
                StreamReader sr = new StreamReader(filepath, Encoding.UTF8, true);
                string line = "";
                while (true)
                {
                    line = sr.ReadLine();
                    if (line == null)
                        break;
                    data.Add(line);
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static bool Write(List<User> data,string filepath, bool append)
        {
            try
            {
                StreamWriter sw = new StreamWriter(filepath, append, Encoding.UTF8);                
                foreach (var item in data)
                {
                    sw.WriteLine(item.ToString());
                }
                sw.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool Write(List<string> data, string filepath, bool append)
        {
            try
            {
                StreamWriter sw = new StreamWriter(filepath, append, Encoding.UTF8);
                foreach (var item in data)
                {
                    sw.WriteLine(item.ToString());
                }
                sw.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool Write(string data, string filepath, bool append)
        {
            try
            {
                StreamWriter sw = new StreamWriter(filepath, append, Encoding.UTF8);
                if(data!="")
                    sw.WriteLine(data);
                else
                    sw.Write(data);
                sw.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<UsersPhoneBook> Read(string Path)
        {
            if (!File.Exists(Path))
            {
                StreamWriter sw = new StreamWriter(Path, true, Encoding.UTF8);
                sw.Close();
            }

            List<UsersPhoneBook> UsersPhone = new List<UsersPhoneBook>();

            String[] AllLines = System.IO.File.ReadAllLines(Path);

            var query = from line in AllLines
                        let data = line.Split(',') //Ayraç;
                        select new
                        {
                            Name = data[0],
                            Surname = data[1],
                            PhoneNumber = data[2],
                            PersonAddress = data[3],
                            PersonDescription = data[4],
                            PersonMail = data[5],
                            userName = data[6],
                            idNum = data[7],
                        };
            foreach (var item in query)
            {
                int idNumber = int.Parse(item.idNum);
                UsersPhoneBook UsersPhoneClass = new UsersPhoneBook(item.Name, item.Surname, item.PhoneNumber, item.PersonAddress, item.PersonDescription, item.PersonMail, item.userName, idNumber);
                UsersPhone.Add(UsersPhoneClass);
            }
            return UsersPhone;
        }

        public static List<UsersNote> ReadNotes(string Path)
        {
            if (!File.Exists(Path))
            {
                StreamWriter sw = new StreamWriter(Path, true, Encoding.UTF8);
                sw.Close();
            }

            List<UsersNote> UsersNotes = new List<UsersNote>();

            String[] AllLines = System.IO.File.ReadAllLines(Path);

            var query = from line in AllLines
                        let data = line.Split(',') //Ayraç;
                        select new
                        {
                            Notes = data[0],
                            userName = data[1],
                            subject = data[2],
                            idNum = data[3],
                        };
            foreach (var item in query)
            {
                int idNumber = int.Parse(item.idNum);
                UsersNote notes = new UsersNote(Base64.Base64Decode(item.Notes.Replace(@"\n", " ")), item.userName, item.subject, idNumber);
                UsersNotes.Add(notes);
            }
            return UsersNotes;
        }
        public static List<Salary> ReadSalary(string Path)
        {
            if (!File.Exists(Path))
            {
                StreamWriter sw = new StreamWriter(Path, true, Encoding.UTF8);
                sw.Close();
            }

            List<Salary> Salaries = new List<Salary>();

            String[] AllLines = System.IO.File.ReadAllLines(Path);

            var query = from line in AllLines
                        let data = line.Split(',') //Ayraç;
                        select new
                        {
                            indexOfDeneyim = int.Parse(data[0]),
                            indexOfYasanilanil = int.Parse(data[1]),
                            indexOfUstOgrenim = int.Parse(data[2]),
                            indexOfYabanciDil = int.Parse(data[3]),
                            indexOfYoneticilik = int.Parse(data[4]),
                            indexOfAileDurumu = int.Parse(data[5]),
                            deneyim = double.Parse(data[6]),
                            yasanilanil = double.Parse(data[7]),
                            ustOgrenim = double.Parse(data[8]),
                            yabanciDil = double.Parse(data[9]),
                            yoneticilik = double.Parse(data[10]),
                            aileDurumu = double.Parse(data[11]),
                            Evli = bool.Parse(data[12]),
                            Yas06 = bool.Parse(data[13]),
                            Yas718 = bool.Parse(data[14]),
                            Yas18ustu = bool.Parse(data[15]),
                            Checkbelge = bool.Parse(data[16]),
                            CheckDiploma = bool.Parse(data[17]),
                            Checkextrabelge = bool.Parse(data[18]),
                            extraBelgeAdet = int.Parse(data[19]),
                            idNumber = int.Parse(data[20]),
                            MaasBilgisi = double.Parse(data[21]),
                        };
            foreach (var item in query)
            {
                Salary newSalaries;
                ReportMaas reportmaas = new ReportMaas();
                Maas maasTMP = new Maas();
                newSalaries = reportmaas.MakeReport(maasTMP,item.indexOfDeneyim, item.indexOfYasanilanil, item.indexOfUstOgrenim, item.indexOfYabanciDil, item.indexOfYoneticilik, item.indexOfAileDurumu, item.deneyim, item.yasanilanil, item.ustOgrenim, item.yabanciDil, item.yoneticilik, item.aileDurumu, item.Evli, item.Yas06, item.Yas718, item.Yas18ustu, item.Checkbelge, item.CheckDiploma, item.Checkextrabelge, item.extraBelgeAdet, item.idNumber, item.MaasBilgisi);
                Salaries.Add(newSalaries);
            }
            return Salaries;
        }
    }
}
