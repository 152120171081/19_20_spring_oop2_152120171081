﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nesne_Lab_Project
{
    public partial class Admin : Form
    {
        public Admin()
        {
            InitializeComponent();
        }
        int tempSelectedUserID = -1;
        private LoginedUser LU=LoginedUser.UserSingleton();
        List<Salary> SalaryForAdmin = new List<Salary>();
        private void Admin_Load(object sender, EventArgs e)
        {
            SalaryForAdmin = FileOperatin.ReadSalary("../../Data/Salary.csv");
            cbbtn_status.DataSource = new string[] { "Admin", "User", "Part-Time-User" };
            foreach (var item in Login.userlist)
            {
                if (!item.Equal(LU.user) && item.Id != 0)
                {
                    lstvw_users.Items.Add(new ListViewItem(new string[] { item.Id.ToString(), item.Name, item.Password, item.Gender, item.Status, item.Mail, Salary.DisplayMaas(SalaryForAdmin,item.Id).ToString() }));                       
                }
            }
            columnHeader1.Dispose();
        }
        int progresValue = 0;
        private void btn_save_Click(object sender, EventArgs e)
        {
            if (tempSelectedUserID == -1)
            {
                MessageBox.Show("Seçim yapmalısınız.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            foreach (var item in Login.userlist)
            {
                if (item.Id.Equals(tempSelectedUserID))
                {
                    item.Status = cbbtn_status.SelectedValue.ToString();
                    item.Name = txt_kadi.Text;
                    if (txt_pass.Text != "")
                    {
                        if (Network_.checkStatus())
                        {
                            tmr_sending.Start();
                            item.Password = Hash.ComputeSha256Hash(txt_pass.Text);
                            lbl_sending.Visible = prgbr_sending.Visible = true;
                            prgbr_sending.Value =progresValue= 0;
                            tmr_sending.Interval = 10;

                            string msg = "Kullanıcı adı:" + item.Name + "\nŞifre: " + txt_pass.Text;
                            Network_.MailSender(item.Mail, "Yeni şifre", msg,ref progresValue);
                        }
                        else
                        {
                            MessageBox.Show("Mail gönderilemedi internet erişiminde gönderilcektir...", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            string data = item.Mail + "," + txt_kadi.Text + "," +txt_pass.Text;
                            FileOperatin.Write(data, "../../Data/Send.csv", false);
                            Anaform.tmr_connection.Start();
                        }
                    }
                    string gender;
                    if (rdibtn_erkek.Checked)
                        gender = "Erkek";
                    else
                        gender = "Kadın";
                    item.Gender=gender;
                    lstvw_users.SelectedItems[0].SubItems[0].Text = item.Name;
                    lstvw_users.SelectedItems[0].SubItems[1].Text = item.Password;
                    lstvw_users.SelectedItems[0].SubItems[2].Text = item.Gender;
                    lstvw_users.SelectedItems[0].SubItems[3].Text = item.Status;
                    lstvw_users.SelectedItems[0].SubItems[4].Text = item.Mail;
                }
            }
            FileOperatin.Write(Login.userlist, "../../Data/users.csv", false);
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }      

        private void lstvw_users_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lstvw_users.SelectedItems.Count == 0)
                return;
            tempSelectedUserID=int.Parse(lstvw_users.SelectedItems[0].SubItems[0].Text);
            txt_kadi.Text = lstvw_users.SelectedItems[0].SubItems[1].Text;
            txt_mail.Text = lstvw_users.SelectedItems[0].SubItems[5].Text;
            if (lstvw_users.SelectedItems[0].SubItems[3].Text.Equals("Erkek"))
                rdibtn_erkek.Checked = true;
            else
                rdibtn_kadin.Checked = true;
            progresValue = 0;
            prgbr_sending.Visible = false;
            lbl_sending.Visible = false;
            lbl_result.Text = "";
        }

        private void txt_pass_Click(object sender, EventArgs e)
        {
            if(DialogResult.Yes== MessageBox.Show("Parolayı değiştirmek istediğinize emin misiniz?.", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                txt_pass.ReadOnly = false;
            }
        }

        private void txt_pass_MouseHover(object sender, EventArgs e)
        {
            ttp_pass.Show("Parolayı değiştirmek için tıklayın.",txt_pass);
            ttp_pass.OwnerDraw = true;
        }

        private void ttp_pass_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            e.DrawBorder();
            e.DrawText();
        }

        private void tmr_sending_Tick(object sender, EventArgs e)
        {
            if (prgbr_sending.Value.Equals(50))
                tmr_sending.Interval = 500;
            else if((prgbr_sending.Value < 50))
                prgbr_sending.Value = progresValue;
            if (prgbr_sending.Value >= 50)
                prgbr_sending.Value += 10;

            if (prgbr_sending.Value==prgbr_sending.Maximum)
            {
                tmr_sending.Stop();
                lbl_result.Text="Yeni şifre yollandı...";
            }
        }       
    }
}
