﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    public class User
    {
        private int id;
        private string username;
        private string pasword;
        private string status;
        private bool remember;
        private string gender = "";
        private string mail;

        public User(int id, string username, string password, string status, string gender,string mail, bool remember)
        {
            this.id = id;
            this.username = username;
            this.pasword = password;
            this.status = status;
            this.remember = remember;
            this.gender = gender;
            this.mail = mail;
        }
        
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string Name
        {
            get { return this.username; }
            set { this.username = value; }
        }

        public string Password
        {
            get { return this.pasword; }
            set { this.pasword = value; }
        }

        public string Status
        {
            get { return this.status; }
            set { this.status = value; }
        }

        public bool Remember
        {
            get { return this.remember; }
            set { this.remember = value; }
        }

        public string Gender
        {
            get { return this.gender; }
            set { this.gender = value; }
        }

        public string Mail
        {
            get { return this.mail; }
            set { this.mail = value; }
        }

        public bool Equal(User u)
        {
            if (u.Id.Equals(this.Id) && u.Name.Equals(this.Name) && u.Password.Equals(this.Password) && u.Status.Equals(this.Status) && u.Mail.Equals(this.mail))  
            {
                return true;
            }
            return false;
        }

        public string ToString()
        {
            return this.id + "," + this.username + "," + this.Password + "," + this.status + "," + this.Gender + "," + this.mail + "," + this.remember;
        }
    }
}
