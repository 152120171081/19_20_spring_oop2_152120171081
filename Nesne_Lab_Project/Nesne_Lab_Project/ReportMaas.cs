﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    public class ReportMaas
    {
        public Salary MakeReport(SalaryBuilder salaryBuilder, int deneyimIndex, int deneyimYasanilanil, int ustogrenim, int yabancidil, int yoneticilik, int ailedurumu, double deneyimValue, double yasanilanilValue, double ustogrenimValue, double dilbilgisiValue, double yoneticilikValue, double ailedurumuValue, bool checkevli, bool check06, bool check718, bool check18ustu,bool checkBelge, bool checkDiploma, bool checkExtraBelge, int extraBelgeAdet,int idNumber,double maasBilgisi)
        {
            salaryBuilder.CreateNewSalary();
            salaryBuilder.SetIndexofdeneyim(deneyimIndex);
            salaryBuilder.SetIndexofyasanilanil(deneyimYasanilanil);
            salaryBuilder.SetIndexofustogrenim(ustogrenim);
            salaryBuilder.SetIndexofyabancidilbilgisi(yabancidil);
            salaryBuilder.SetIndexofyoneticilik(yoneticilik);
            salaryBuilder.SetIndexofailedurumu(ailedurumu);

            salaryBuilder.SetDeneyim(deneyimValue);
            salaryBuilder.SetYasanilanil(yasanilanilValue);
            salaryBuilder.SetUstogrenim(ustogrenimValue);
            salaryBuilder.SetYabancidilbilgisi(dilbilgisiValue);
            salaryBuilder.SetYoneticilik(yoneticilikValue);
            salaryBuilder.SetAiledurumu(ailedurumuValue);

            salaryBuilder.SetCheckevli(checkevli);
            salaryBuilder.SetCheck06(check06);
            salaryBuilder.SetCheck718(check718);
            salaryBuilder.SetCheck18ustu(check18ustu);

            salaryBuilder.SetCheckBelge(checkBelge);
            salaryBuilder.SetCheckDiploma(checkDiploma);
            salaryBuilder.SetCheckExtraBelge(checkExtraBelge);
            salaryBuilder.SetExtraBelgeAdet(extraBelgeAdet);

            salaryBuilder.SetIdNumber(idNumber);
            salaryBuilder.SetMaasBilgisi(maasBilgisi);

            return salaryBuilder.GetSalary();
        }
    }
}
