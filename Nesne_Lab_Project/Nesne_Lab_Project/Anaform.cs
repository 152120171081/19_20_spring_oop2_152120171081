﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nesne_Lab_Project
{
    public partial class Anaform : Form
    {
        public static Timer tmr_connection=new Timer();

        public static List<string> messages = new List<string>();
        public Anaform()
        {
            InitializeComponent();
            tmr_connection.Interval = 60000;
            tmr_connection.Tick += new System.EventHandler(this.tmr_connection_Tick);
        }

        LoginedUser LU;

        private void Anaform_Load(object sender, EventArgs e)
        {
            try
            {
                tmr_connection.Start();

                this.Visible=false;
                Login l = new Login();
                l.ShowDialog();
                if (Login.exit)
                {
                    this.Close();
                    return;
                }

                LU = LoginedUser.UserSingleton();
                StatusCheck();
                ReminderMessageSet();
                tmr_ReminderShow.Start();
                lbl_kullanici.Text = "Hoş geldin " + LU.user.Name + ":-) :-)";
                if (!this.Visible && LU.user!=null)
                    this.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eror", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsbtn_Cikis_Click(object sender, EventArgs e)
        {
            if (DialogResult.No == MessageBox.Show("Oturum kapatmak istediğinize emin misiniz?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                return;
            Login.LogOUT = true;
            Anaform_Load(null, null);
        }

        private void tsbtn_saveUsers_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ofd = new SaveFileDialog();
                string Path = "";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    Path = ofd.FileName;
                    int counter = 1;
                    foreach (var item in Login.userlist)
                    {
                        FileOperatin.Write("User" + counter + ") " + item.Name + "\t Password: " + item.Password + "\t Status: " + item.Status, Path + ".tvs", true);
                        counter++;
                    }
                    MessageBox.Show("Kayıt tamamlandı..", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eror", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void StatusCheck()
        {
            if(LU.user.Status.Equals("Admin"))
            {
                tsbtn_Admin.Visible = true;
                tsbtn_Admin.Enabled = true;
                return;
            }
            tsbtn_Admin.Visible = false;
            tsbtn_Admin.Enabled = false;
        }

        private void tsbtn_Admin_Click(object sender, EventArgs e)
        {
            if (LU.user.Status.Equals("Admin"))
            {
                Admin A = new Admin();
                A.ShowDialog();
            }
            else
            {
                MessageBox.Show("Erişim hakkınız yok. Bu butonu görüyorsanız programda yanlışlık vardır. Admin(ler)e bilgidiriniz...", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void tsbtn_PhoneBook_Click(object sender, EventArgs e)
        {
            PhoneBook phone = new PhoneBook();
            phone.ShowDialog();
        }

        private void tsbtn_userInfo_Click(object sender, EventArgs e)
        {
            User_Info UI = new User_Info();
            UI.ShowDialog();
        }

        private void tsbtn_UsersNote_Click(object sender, EventArgs e)
        {
            UsersNoteForm notes = new UsersNoteForm();
            notes.ShowDialog();
        }
        
        private void SendMail()
        {
            List<string> queue = new List<string>();
            FileOperatin.Read("../../Data/Send.csv", queue);
            if (queue.Count.Equals(0))
            {
                tmr_connection.Stop();
                return;
            }
            bool check = false;
            foreach (var item in queue)
            {
                string[] temp = item.Split(',');

                string msg = "Kullanıcı adı:" + temp[1] + "\nŞifre: " + temp[2];
                if (Network_.MailSender(temp[0], "Yeni şifre", msg))
                {
                    check = true;
                    foreach (var user in Login.userlist)
                    {
                        if (user.Name == temp[1])
                        {
                            user.Password = Hash.ComputeSha256Hash(temp[2]);
                            break;
                        }
                    }
                }
            }
            if (check)
            {
                MessageBox.Show("Önceden gönderilemeyen mailler gönderildi...", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FileOperatin.Write(Login.userlist, "../../Data/users.csv", false);
                FileOperatin.Write("", "../../Data/Send.csv", false);
            }
        }

        private void tmr_connection_Tick(object sender, EventArgs e)
        {
            if (Network_.checkStatus())
            {
                SendMail();
                tmr_connection.Stop();
            }
        }

        private void tsbtn_Salary_Click(object sender, EventArgs e)
        {
            SalaryForm CalculateSalary = new SalaryForm();
            CalculateSalary.ShowDialog();
        }

        private void tsbtn_AddReminderNote_Click(object sender, EventArgs e)
        {
            Reminder r = new Reminder();
            r.ShowDialog();
        }

        private void ReminderMessageSet()
        {
            FileOperatin.Read("../../Data/Reminder.csv",messages);
            int count = 0;
            foreach (string item in messages)
            {
                string[] temp = item.Split(',');
                if (temp[7].Equals("Silindi"))
                    continue;
                Message M = new Message();
                M.Id = int.Parse(temp[0]);
                M.UserId = int.Parse(temp[1]);
                M.Type = temp[2];
                M.Title = temp[3];
                M.Description= temp[4];
                M.Time = new DateTime(long.Parse(temp[5]));
                M.Status = temp[6] == "True" ? true : false;
                if (M.UserId.Equals(LU.user.Id)) 
                    LU.MessageAdd(M,count);
                count++;
            }
        }

        private void tmr_ReminderShow_Tick(object sender, EventArgs e)
        {
            foreach (KeyValuePair<Message,int> item in LU.ReminderMessages)
            {
                if (item.Key.Status && item.Key.Time <= DateTime.Now)
                {
                    Reminder_Message RM = new Reminder_Message();
                    RM.M = item.Key;
                    RM.Show();
                    item.Key.Status = false;
                    messages[item.Value] = item.Key.ToString()+",Kullanımda";
                    FileOperatin.Write(messages, "../../Data/Reminder.csv", false);
                }
                else if (item.Key.Time > DateTime.Now)
                    break;
            }
        }

        private void Anaform_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult.No == MessageBox.Show("Çıkış yapmak istediğinize emin misiniz?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                e.Cancel = true;
                return;
            }
        }
    }
}
