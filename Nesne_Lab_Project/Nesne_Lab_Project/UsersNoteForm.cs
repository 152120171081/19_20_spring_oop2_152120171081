﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Nesne_Lab_Project
{
    public partial class UsersNoteForm : Form
    {
        public UsersNoteForm()
        {
            InitializeComponent();
        }
        List<UsersNote> Notes = new List<UsersNote>();
        LoginedUser LU = LoginedUser.UserSingleton();
        private void UsersNoteForm_Load(object sender, EventArgs e)
        {
            lstvw_Usersnote.Items.Clear();
            Notes = FileOperatin.ReadNotes("../../Data/UsersNote.csv");
            int count = 1;
            foreach (var item in Notes)
            {
                if (item.IdNumber == LU.user.Id)
                {
                    lstvw_Usersnote.Items.Add(new ListViewItem(new string[] { count.ToString() , item.Subject }));
                    count++;
                }
            }
        }
        bool subjectControl(string txt)
        { 
            foreach (var item in Notes)
            {
                if (item.Subject == txt)
                {
                   return true;
                }
            }
            return false;
        }
        private void btn_AddNotes_Click(object sender, EventArgs e)
        {
            if (subjectControl(txt_Subject.Text) == true)
            {
                MessageBox.Show("Eklemek istediğiniz notun başlığı daha önce eklediğiniz notlardan farklı olmalıdır...\nKarışmaması için lütfen başlığı değiştiriniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (lstvw_Usersnote.SelectedItems.Count == 0)
            {
                if (txt_Notes.Text != "" && txt_Notes.Text != " " && txt_Subject.Text != "")
                {
                    UsersNote UserNoteDetails = new UsersNote(txt_Notes.Text, LU.user.Name, txt_Subject.Text, LU.user.Id);

                    Notes.Add(UserNoteDetails);
                    string data =  Base64.toBase64(txt_Notes.Text) + "," + LU.user.Name + "," + txt_Subject.Text + "," + LU.user.Id;
                    if (FileOperatin.Write(data, "../../Data/UsersNote.csv", true))
                    {
                        lbl_StatusNotes.Text = "Ekleme başarılı";
                        lbl_StatusNotes.ForeColor = Color.Green;
                        UsersNoteForm_Load(sender, e);
                        txt_Subject.Text = "";
                        txt_Notes.Text = "";
                    }
                    else
                    {
                        lbl_StatusNotes.Text = "Ekleme başarısız";
                        lbl_StatusNotes.ForeColor = Color.Red;
                    }
                }
                else
                {
                    MessageBox.Show("Not eklemek için Sağ tarafta bulunan kutucuğa eklemek istediğiniz notu yazınız...\nNotunuz ve Başlık sadece boşluktan oluşmamalıdır.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Not eklemek istediğinizde lütfen yandaki listeden herhangi bir not seçmeyiniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        void DosyaDuzeltme(string Path)
        {
            string myPath = Path;
            System.IO.File.WriteAllText(myPath, string.Empty);

            StreamWriter sw = new StreamWriter(myPath, true);

            for (int i = 0; i < Notes.Count(); i++)
            {
                sw.WriteLine("{0},{1},{2},{3}", Base64.toBase64(Notes[i].Information), Notes[i].UserName , Notes[i].Subject, Notes[i].IdNumber);
            }
            sw.Close();
        }
        private void btn_Deletenotes_Click(object sender, EventArgs e)
        {
            if (lstvw_Usersnote.SelectedItems.Count == 1)
            {
                int count = 0;
                foreach (var item in Notes)
                {
                   if ((item.IdNumber == LU.user.Id) && (item.Subject == lstvw_Usersnote.SelectedItems[0].SubItems[1].Text))
                   {
                       Notes.RemoveAt(count);
                       break;
                   }
                  count++;
                }
                lstvw_Usersnote.Items.Remove(lstvw_Usersnote.SelectedItems[0]);
                DosyaDuzeltme("../../Data/UsersNote.csv");
                txt_Notes.Text = "";
                txt_Subject.Text = "";
                UsersNoteForm_Load(sender, e);
            }
            else
            {
                MessageBox.Show("Silmek istediğiniz notu lütfen listeden seçiniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Updatenotes_Click(object sender, EventArgs e)
        {
            if (lstvw_Usersnote.SelectedItems.Count == 1)
            {
                if (txt_Notes.Text != "" && txt_Notes.Text != " ")
                {
                    foreach (var item in Notes)
                    {
                        if ((item.IdNumber == LU.user.Id) && (item.Subject == (lstvw_Usersnote.SelectedItems[0].SubItems[1].Text)))
                        {
                            if(subjectControl(txt_Subject.Text) == true && lstvw_Usersnote.SelectedItems[0].SubItems[1].Text != txt_Subject.Text)
                            {
                                MessageBox.Show("Güncellemek istediğiniz notun başlığı daha önce eklediğiniz notlardan farklı olmalıdır...\nKarışmaması için lütfen başlığı değiştiriniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                txt_Subject.Text = "";
                                txt_Notes.Text = "";
                            }
                            else
                            {
                                item.Information = txt_Notes.Text;
                                item.Subject = txt_Subject.Text;
                                lstvw_Usersnote.Items[lstvw_Usersnote.FocusedItem.Index].SubItems[1].Text = txt_Subject.Text;
                            }   
                        }
                    }
                    DosyaDuzeltme("../../Data/UsersNote.csv");
                }
                else
                {
                    MessageBox.Show("Not eklemek için Sağ tarafta bulunan kutucuğa eklemek istediğiniz notu yazınız...\nNotunuz sadece boşluktan oluşmamalıdır.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Güncellemek istediğiniz notu lütfen listeden seçiniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void lstvw_Usersnote_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lstvw_Usersnote.SelectedItems.Count == 0)
                return;

            foreach (var item in Notes)
            {
                if ((item.IdNumber == LU.user.Id) && (item.Subject == lstvw_Usersnote.SelectedItems[0].SubItems[1].Text))
                {
                    txt_Notes.Text = item.Information;
                    txt_Subject.Text = item.Subject;
                }
            }
        }
        private void txt_Notes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)//Enter
            {
                e.Handled = true;
                SelectNextControl(txt_Subject, true, true, true, true);
            }
        }
    }
}
