﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nesne_Lab_Project
{
    public partial class SalaryForm : Form
    {
        public SalaryForm()
        {
            InitializeComponent();
        }
        List<Salary> SalaryList = new List<Salary>();
        LoginedUser LU = LoginedUser.UserSingleton();
        Salary salary;
        ReportMaas reportmaas = new ReportMaas();
        void findIndex(ref int indexDeneyim, ref int indexYasanilanil, ref int indexUstogrenim, ref int indexYabancidil, ref int indexYoneticilik, ref int indexAiledurumu)
        {
            if (cmbx_deneyim.SelectedIndex == -1) { cmbx_deneyim.SelectedIndex = 0; }
            if (cmbx_ailedurumu.SelectedIndex == -1) { cmbx_ailedurumu.SelectedIndex = 0; }
            if (cmbx_ustogrenim.SelectedIndex == -1) { cmbx_ustogrenim.SelectedIndex = 0; }
            if (cmbx_yabancidil.SelectedIndex == -1) { cmbx_yabancidil.SelectedIndex = 0; }
            if (cmbx_yoneticilik.SelectedIndex == -1) { cmbx_yoneticilik.SelectedIndex = 0; }
            if (cmbx_yasanilanil.SelectedIndex == -1) { cmbx_yasanilanil.SelectedIndex = 11; }

            indexDeneyim = cmbx_deneyim.SelectedIndex;
            indexYasanilanil = cmbx_yasanilanil.SelectedIndex;
            indexUstogrenim = cmbx_ustogrenim.SelectedIndex;
            indexYabancidil = cmbx_yabancidil.SelectedIndex;
            indexYoneticilik = cmbx_yoneticilik.SelectedIndex;
            indexAiledurumu = cmbx_ailedurumu.SelectedIndex;
        }
        void findValue(ref double deneyim, ref double yasanilanil, ref double ustogrenim, ref double yabancidil, ref double yoneticilik, ref double ailedurumu, ref bool Checkevli, ref bool Check06, ref bool Check718, ref bool Check18ustu,ref bool checkBelge, ref bool checkDiploma, ref bool checkExtraBelge, ref int belgeAdet,ref double maasBilgisi)
        {
            if (cmbx_deneyim.SelectedIndex == 0) { deneyim = 0.00; }
            if (cmbx_deneyim.SelectedIndex == 1) { deneyim = 0.60; }
            if (cmbx_deneyim.SelectedIndex == 2) { deneyim = 1.00; }
            if (cmbx_deneyim.SelectedIndex == 3) { deneyim = 1.20; }
            if (cmbx_deneyim.SelectedIndex == 4) { deneyim = 1.35; }
            if (cmbx_deneyim.SelectedIndex == 5) { deneyim = 1.50; }

            if (cmbx_yasanilanil.SelectedIndex == 0) { yasanilanil = 0.15; }
            if (cmbx_yasanilanil.SelectedIndex == 1 || cmbx_yasanilanil.SelectedIndex == 2) { yasanilanil = 0.10; }
            if (cmbx_yasanilanil.SelectedIndex == 3 || cmbx_yasanilanil.SelectedIndex == 4) { yasanilanil = 0.05; }
            if (cmbx_yasanilanil.SelectedIndex == 5 || cmbx_yasanilanil.SelectedIndex == 6 || cmbx_yasanilanil.SelectedIndex == 7 || cmbx_yasanilanil.SelectedIndex == 8 || cmbx_yasanilanil.SelectedIndex == 9 || cmbx_yasanilanil.SelectedIndex == 10) { yasanilanil = 0.03; }
            if (cmbx_yasanilanil.SelectedIndex == 11) { yasanilanil = 0; }

            if (cmbx_ustogrenim.SelectedIndex == 0) { ustogrenim = 0.00; }
            if (cmbx_ustogrenim.SelectedIndex == 1) { ustogrenim = 0.10; }
            if (cmbx_ustogrenim.SelectedIndex == 2) { ustogrenim = 0.30; }
            if (cmbx_ustogrenim.SelectedIndex == 3) { ustogrenim = 0.35; }
            if (cmbx_ustogrenim.SelectedIndex == 4) { ustogrenim = 0.05; }
            if (cmbx_ustogrenim.SelectedIndex == 5) { ustogrenim = 0.15; }

            if (cmbx_yabancidil.SelectedIndex == 0) { yabancidil = 0.00; }
            if (cmbx_yabancidil.SelectedIndex == 1) { yabancidil = 0.20; }
            if (cmbx_yabancidil.SelectedIndex == 2) { yabancidil = 0.20; }
            if (cmbx_yabancidil.SelectedIndex == 3) { yabancidil = 0.05; }
            if (cmbx_yabancidil.SelectedIndex == 4)
            {
                groupBox_yabancidilbilgisi.Visible = true;
                yabancidil = 0.00;
                if(checkBox_belgelendirilmisBelge.Checked == true) { yabancidil += 0.20; checkBelge = true; }
                if(checkBox_MezuniyetBelge.Checked == true) { yabancidil += 0.20; checkDiploma = true; }
                if(checkBox_extraBelge.Checked == true)
                {
                    checkExtraBelge = true;
                    groupBox_extraBelge.Visible = true;
                    try
                    {
                        belgeAdet = int.Parse(textBox_belgeAdetBilgisi.Text);
                        if(belgeAdet <= 0)
                        {
                            MessageBox.Show("Belge adet kismina girdiginiz sayi 0 dan buyuk olmali...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox_belgeAdetBilgisi.Text = "";
                            belgeAdet = 0;
                        }
                        yabancidil += belgeAdet * 0.05;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Lutfen belge kismina bir sayi giriniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                if(checkBox_extraBelge.Checked == false)
                {
                    checkExtraBelge = false;
                    belgeAdet = 0;
                    groupBox_extraBelge.Visible = false;
                }
            }
            if(cmbx_yabancidil.SelectedIndex != 4)
            {
                checkBelge = false;
                checkDiploma = false;
                groupBox_yabancidilbilgisi.Visible = false;
            }

            if (cmbx_yoneticilik.SelectedIndex == 0) { yoneticilik = 0.00; }
            if (cmbx_yoneticilik.SelectedIndex == 1) { yoneticilik = 0.50; }
            if (cmbx_yoneticilik.SelectedIndex == 2) { yoneticilik = 0.75; }
            if (cmbx_yoneticilik.SelectedIndex == 3) { yoneticilik = 0.85; }
            if (cmbx_yoneticilik.SelectedIndex == 4) { yoneticilik = 1.00; }
            if (cmbx_yoneticilik.SelectedIndex == 5) { yoneticilik = 0.40; }
            if (cmbx_yoneticilik.SelectedIndex == 6) { yoneticilik = 0.60; }

            if (cmbx_ailedurumu.SelectedIndex == 0) { ailedurumu = 0.00; }
            if (cmbx_ailedurumu.SelectedIndex == 1) { ailedurumu = 0.20; }
            if (cmbx_ailedurumu.SelectedIndex == 2) { ailedurumu = 0.20; }
            if (cmbx_ailedurumu.SelectedIndex == 3) { ailedurumu = 0.30; }
            if (cmbx_ailedurumu.SelectedIndex == 4) { ailedurumu = 0.40; }
            if (cmbx_ailedurumu.SelectedIndex == 5)
            {
                ailedurumu = 0;
                grpbx_Ailedurumu.Visible = true;
                if (checkBox_evli_esicalismiyor.Checked == true) { ailedurumu += 0.20; Checkevli = true; }
                if (checkBox_06arasicocuk.Checked == true) { ailedurumu += 0.20; Check06 = true; }
                if (checkBox_7_18arasicocuk.Checked == true) { ailedurumu += 0.30; Check718 = true; }
                if (checkBox_18ustucocuk.Checked == true) { ailedurumu += 0.40; Check18ustu = true; }
            }
            if (cmbx_ailedurumu.SelectedIndex != 5)
            {
                Checkevli = false;
                Check06 = false;
                Check718 = false;
                Check18ustu = false;
                grpbx_Ailedurumu.Visible = false;
            }
            if(LU.user.Status == "Part-Time-User")
            {
                maasBilgisi = (5000 * (deneyim + yasanilanil + ustogrenim + yabancidil + yoneticilik + ailedurumu + 1)) / 2;
            }
            if(LU.user.Status != "Part-Time-User")
            {
                maasBilgisi = (5000 * (deneyim + yasanilanil + ustogrenim + yabancidil + yoneticilik + ailedurumu + 1));
            }
        }
        private void SalaryForm_Load(object sender, EventArgs e)
        {
            groupBox_yabancidilbilgisi.Visible = false;
            groupBox_extraBelge.Visible = false;
            grpbx_Ailedurumu.Visible = false;

            SalaryList = FileOperatin.ReadSalary("../../Data/Salary.csv");
            foreach(var item in SalaryList)
            {
                if(item.IdNumber == LU.user.Id)
                {
                    cmbx_deneyim.SelectedIndex = item.IndexOfDeneyim;
                    cmbx_ustogrenim.SelectedIndex = item.IndexOfUstOgrenim;
                    cmbx_yabancidil.SelectedIndex = item.IndexOfYabanciDil;
                    if (cmbx_yabancidil.SelectedIndex == 4)
                    {
                        groupBox_yabancidilbilgisi.Visible = true;
                        checkBox_belgelendirilmisBelge.Checked = item.CheckBelge;
                        checkBox_MezuniyetBelge.Checked = item.CheckDiploma;
                        checkBox_extraBelge.Checked = item.CheckExtraBelge;
                        if(item.CheckExtraBelge == true)
                        {
                            groupBox_extraBelge.Visible = true;
                            textBox_belgeAdetBilgisi.Text = item.ExtraBelgeAdet.ToString();
                        }
                        else
                        {
                            groupBox_extraBelge.Visible = false;
                        }
                    }
                    else
                    {
                        groupBox_yabancidilbilgisi.Visible = false;
                    }
                    cmbx_yoneticilik.SelectedIndex = item.IndexOfYoneticilik;
                    cmbx_yasanilanil.SelectedIndex = item.IndexOfYasanilanil;
                    cmbx_ailedurumu.SelectedIndex = item.IndexOfAileDurumu;
                    if(cmbx_ailedurumu.SelectedIndex == 5)
                    {
                        grpbx_Ailedurumu.Visible = true;
                        checkBox_evli_esicalismiyor.Checked = item.Checkevli;
                        checkBox_06arasicocuk.Checked = item.Check06;
                        checkBox_7_18arasicocuk.Checked = item.Check718;
                        checkBox_18ustucocuk.Checked = item.Check18ustu;
                    }
                    else
                    {
                        grpbx_Ailedurumu.Visible = false;
                    }
                }
            }
        }
        private void checkBox_extraBelge_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox_extraBelge.Checked == true)
            {
                groupBox_extraBelge.Visible = true;
            }
            else
            {
                groupBox_extraBelge.Visible = false;
            }
        }
        void DosyaDuzeltme(string Path)
        {
            string myPath = Path;
            System.IO.File.WriteAllText(myPath, string.Empty);

            StreamWriter sw = new StreamWriter(myPath, true);

            for (int i = 0; i < SalaryList.Count(); i++)
            {
                sw.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21}", SalaryList[i].IndexOfDeneyim, SalaryList[i].IndexOfYasanilanil, SalaryList[i].IndexOfUstOgrenim, SalaryList[i].IndexOfYabanciDil, SalaryList[i].IndexOfYoneticilik, SalaryList[i].IndexOfAileDurumu, SalaryList[i].Deneyim, SalaryList[i].Yasanilanil, SalaryList[i].UstOgrenim, SalaryList[i].Yabancidilbilgisi , SalaryList[i].Yoneticilik, SalaryList[i].AileDurumu, SalaryList[i].Checkevli, SalaryList[i].Check06, SalaryList[i].Check718, SalaryList[i].Check18ustu, SalaryList[i].CheckBelge, SalaryList[i].CheckDiploma, SalaryList[i].CheckExtraBelge, SalaryList[i].ExtraBelgeAdet, SalaryList[i].IdNumber, SalaryList[i].MaasBilgisi);
            }
            sw.Close();
        }
        private void button_Kaydet_Click(object sender, EventArgs e)
        {
            int indexDeneyim = 0, indexYasanilanil = 0, indexUstogrenim = 0, indexYabancidil = 0, indexYoneticilik = 0, indexAiledurumu = 0, belgeAdet = 0;
            double deneyim = 0, yasanilanil = 0, ustogrenim = 0, yabancidil = 0, yoneticilik = 0, ailedurumu = 0, maasBilgisi = 0;
            bool Checkevli = false, Check06 = false, Check718 = false, Check18ustu = false;
            bool checkBelge = false, checkDiploma = false, checkExtraBelge = false;
            int idNumber = LU.user.Id;
            int count = 0;
            foreach (var item in SalaryList)
            {
                if (item.IdNumber == LU.user.Id)
                {
                    count = 1;
                    break;
                }
            }
            if (SalaryList.Count != 0)
            {
                if (count >= 1)
                {
                    MessageBox.Show("Daha once bilgileriniz girildi. Lutfen sadece guncelleyiniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    Maas maas = new Maas();

                    findIndex(ref indexDeneyim, ref indexYasanilanil, ref indexUstogrenim, ref indexYabancidil, ref indexYoneticilik, ref indexAiledurumu);
                    findValue(ref deneyim, ref yasanilanil, ref ustogrenim, ref yabancidil, ref yoneticilik, ref ailedurumu, ref Checkevli, ref Check06, ref Check718, ref Check18ustu, ref checkBelge, ref checkDiploma, ref checkExtraBelge, ref belgeAdet, ref maasBilgisi);

                    salary = reportmaas.MakeReport(maas, indexDeneyim, indexYasanilanil, indexUstogrenim, indexYabancidil, indexYoneticilik, indexAiledurumu, deneyim, yasanilanil, ustogrenim, yabancidil, yoneticilik, ailedurumu, Checkevli, Check06, Check718, Check18ustu, checkBelge, checkDiploma, checkExtraBelge, belgeAdet, idNumber, maasBilgisi);

                    SalaryList.Add(salary);
                    DosyaDuzeltme("../../Data/Salary.csv");
                }
            }
            else
            {
                Maas maas = new Maas();

                findIndex(ref indexDeneyim, ref indexYasanilanil, ref indexUstogrenim, ref indexYabancidil, ref indexYoneticilik, ref indexAiledurumu);
                findValue(ref deneyim, ref yasanilanil, ref ustogrenim, ref yabancidil, ref yoneticilik, ref ailedurumu, ref Checkevli, ref Check06, ref Check718, ref Check18ustu, ref checkBelge, ref checkDiploma, ref checkExtraBelge, ref belgeAdet, ref maasBilgisi);

                salary = reportmaas.MakeReport(maas, indexDeneyim, indexYasanilanil, indexUstogrenim, indexYabancidil, indexYoneticilik, indexAiledurumu, deneyim, yasanilanil, ustogrenim, yabancidil, yoneticilik, ailedurumu, Checkevli, Check06, Check718, Check18ustu, checkBelge, checkDiploma, checkExtraBelge, belgeAdet, idNumber, maasBilgisi);

                SalaryList.Add(salary);
                DosyaDuzeltme("../../Data/Salary.csv");
            }
        }

        private void button_Guncelle_Click(object sender, EventArgs e)
        {
            foreach(var item in SalaryList)
            {
                if(item.IdNumber == LU.user.Id)
                {
                    int indexDeneyim = 0, indexYasanilanil = 0, indexUstogrenim = 0, indexYabancidil = 0, indexYoneticilik = 0, indexAiledurumu = 0, belgeAdet = 0;
                    double deneyim = 0, yasanilanil = 0, ustogrenim = 0, yabancidil = 0, yoneticilik = 0, ailedurumu = 0, maasBilgisi = 0;
                    bool Checkevli = false, Check06 = false, Check718 = false, Check18ustu = false;
                    bool checkBelge = false, checkDiploma = false, checkExtraBelge = false;

                    findIndex(ref indexDeneyim, ref indexYasanilanil, ref indexUstogrenim, ref indexYabancidil, ref indexYoneticilik, ref indexAiledurumu);
                    findValue(ref deneyim, ref yasanilanil, ref ustogrenim, ref yabancidil, ref yoneticilik, ref ailedurumu, ref Checkevli, ref Check06, ref Check718, ref Check18ustu, ref checkBelge, ref checkDiploma, ref checkExtraBelge, ref belgeAdet, ref maasBilgisi);

                    item.IndexOfDeneyim = indexDeneyim;
                    item.IndexOfYasanilanil = indexYasanilanil;
                    item.IndexOfUstOgrenim = indexUstogrenim;
                    item.IndexOfYabanciDil = indexYabancidil;
                    item.IndexOfYoneticilik = indexYoneticilik;
                    item.IndexOfAileDurumu = indexAiledurumu;

                    item.Deneyim = deneyim;
                    item.Yasanilanil = yasanilanil;
                    item.UstOgrenim = ustogrenim;
                    item.Yabancidilbilgisi = yabancidil;
                    item.Yoneticilik = yoneticilik;
                    item.AileDurumu = ailedurumu;

                    item.Checkevli = Checkevli;
                    item.Check06 = Check06;
                    item.Check718 = Check718;
                    item.Check18ustu = Check18ustu;
                    item.CheckBelge = checkBelge;
                    item.CheckDiploma = checkDiploma;
                    item.CheckExtraBelge = checkExtraBelge;
                    item.ExtraBelgeAdet = belgeAdet;
                    item.MaasBilgisi = maasBilgisi;
                }
            }
            DosyaDuzeltme("../../Data/Salary.csv");
        }

        private void cmbx_ailedurumu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbx_ailedurumu.SelectedIndex == 5)
            {
                grpbx_Ailedurumu.Visible = true;
            }
            else
            {
                grpbx_Ailedurumu.Visible = false;
            }
        }

        private void cmbx_yabancidil_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbx_yabancidil.SelectedIndex == 4)
            {
                groupBox_yabancidilbilgisi.Visible = true;
            }
            else
            {
                groupBox_yabancidilbilgisi.Visible = false;
            }
        }
    }
}
