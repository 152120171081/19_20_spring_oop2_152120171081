﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    public class Maas : SalaryBuilder
    {
        public override void SetIndexofdeneyim(int index)
        {
            SalaryCalculate.IndexOfDeneyim = index;
        }
        public override void SetIndexofyasanilanil(int index)
        {
            SalaryCalculate.IndexOfYasanilanil = index;
        }
        public override void SetIndexofustogrenim(int index)
        {
            SalaryCalculate.IndexOfUstOgrenim = index;
        }
        public override void SetIndexofyabancidilbilgisi(int index)
        {
            SalaryCalculate.IndexOfYabanciDil = index;
        }
        public override void SetIndexofyoneticilik(int index)
        {
            SalaryCalculate.IndexOfYoneticilik = index;
        }
        public override void SetIndexofailedurumu(int index)
        {
            SalaryCalculate.IndexOfAileDurumu = index;
        }
        public override void SetDeneyim(double value)
        {
            SalaryCalculate.Deneyim = value;
        }
        public override void SetYasanilanil(double value)
        {
            SalaryCalculate.Yasanilanil = value;
        }
        public override void SetUstogrenim(double value)
        {
            SalaryCalculate.UstOgrenim = value;
        }
        public override void SetYabancidilbilgisi(double value)
        {
            SalaryCalculate.Yabancidilbilgisi = value;
        }
        public override void SetYoneticilik(double value)
        {
            SalaryCalculate.Yoneticilik = value;
        }
        public override void SetAiledurumu(double value)
        {
            SalaryCalculate.AileDurumu = value;
        }
        public override void SetCheckevli(bool boolValue)
        {
            SalaryCalculate.Checkevli = boolValue;
        }
        public override void SetCheck06(bool boolValue)
        {
            SalaryCalculate.Check06 = boolValue;
        }
        public override void SetCheck718(bool boolValue)
        {
            SalaryCalculate.Check718 = boolValue;
        }
        public override void SetCheck18ustu(bool boolValue)
        {
            SalaryCalculate.Check18ustu = boolValue;
        }
        public override void SetCheckBelge(bool boolValue)
        {
            SalaryCalculate.CheckBelge = boolValue;
        }
        public override void SetCheckDiploma(bool boolValue)
        {
            SalaryCalculate.CheckDiploma = boolValue;
        }
        public override void SetCheckExtraBelge(bool boolValue)
        {
            SalaryCalculate.CheckExtraBelge = boolValue;
        }
        public override void SetExtraBelgeAdet(int Value)
        {
            SalaryCalculate.ExtraBelgeAdet = Value;
        }

        public override void SetIdNumber(int idNumber)
        {
            SalaryCalculate.IdNumber = idNumber;
        }
        public override void SetMaasBilgisi(double maasBilgisi)
        {
            SalaryCalculate.MaasBilgisi = maasBilgisi;
        }
    }
}
