﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    public class Datas
    {
        public string Kadi { get; set; }
        public string Mail { get; set; }
        public string Gender { get; set; }
        public string Pass { get; set; }
        public string Photo { get; set; }

        public DatasMemento Backup()
        {
            return new DatasMemento
            {
                Kadi = this.Kadi,

                Mail = this.Mail,
                Gender = this.Gender,
                Pass = this.Pass,
                Photo = this.Photo
            };
        }

        public void ReturnBack(DatasMemento datas)
        {
            this.Kadi = datas.Kadi;
            this.Mail = datas.Mail;
            this.Gender = datas.Gender;
            this.Pass = datas.Pass;
            this.Photo = datas.Photo;
        }
    }

    public class DatasMemento
    {
        public string Kadi { get; set; }
        public string Mail { get; set; }
        public string Gender { get; set; }
        public string Pass { get; set; }
        public string Photo { get; set; }
    }
    public class DatasCareTaker
    {
        public DatasMemento Memento { get; set; }
    }
}
