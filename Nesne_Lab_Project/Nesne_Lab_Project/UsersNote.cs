﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    public class UsersNote
    {
        private string subject;
        private string information;
        private string userName;
        private int idNumber;
        public UsersNote(string information, string userName , string subject, int idNumber)
        {
            this.information = information;
            this.userName = userName;
            this.subject = subject;
            this.idNumber = idNumber;
        }
        public string Subject
        {
            get { return this.subject; }
            set { this.subject = value; }
        }
        public string Information
        {
            get { return this.information; }
            set { this.information = value; }
        }
        public string UserName
        {
            get { return this.userName; }
            set { this.userName = value; }
        }
        public int IdNumber
        {
            get { return this.idNumber; }
            set { this.idNumber = value; }
        }
    }
}
