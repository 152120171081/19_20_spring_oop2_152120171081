﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    public class Message
    {
        private int id;
        private int userId;
        private string type;
        private string title;
        private string description;
        private DateTime time;
        private bool status;
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public int UserId
        {
            get { return this.userId; }
            set { this.userId = value; }
        }

        public string Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        
        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }

        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }

        public bool Status
        {
            get { return this.status; }
            set { this.status = value; }
        }

        public static bool operator == (Message M, Message _M)
        {
            return M.Time == _M.time;
        }

        public static bool operator !=(Message M, Message _M)
        {
            return M.Time != _M.time;
        }

        public static bool operator <(Message M, Message _M)
        {
            return M.Time < _M.time;
        }

        public static bool operator >(Message M, Message _M)
        {
            return M.Time > _M.time;
        }

        public static bool operator <=(Message M, Message _M)
        {
            return M.Time <= _M.time;
        }

        public static bool operator >=(Message M, Message _M)
        {
            return M.Time >= _M.time;
        }

        public bool Equals(Message _M)
        {
            return _M != null && this.time == _M.Time;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Message);
        }

        public override int GetHashCode()
        {
            var hashCode = 352033288;
            hashCode = hashCode * ((-1521134295) + this.time.GetHashCode());
            return hashCode;
        }

        public override string ToString()
        {
            return this.id + "," + this.userId + "," + this.type + "," + this.title + "," + this.description + "," + this.time.Ticks.ToString() + "," + this.status;
        }    
    }    
}
