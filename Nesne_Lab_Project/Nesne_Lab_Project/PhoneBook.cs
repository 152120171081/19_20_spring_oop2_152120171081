﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Nesne_Lab_Project
{
    public partial class PhoneBook : Form
    {
        public PhoneBook()
        {
            InitializeComponent();
        }
        List<UsersPhoneBook> UsersPhone = new List<UsersPhoneBook>();
        LoginedUser LU = LoginedUser.UserSingleton();
        private void PhoneBook_Load(object sender, EventArgs e)
        {
            lstvw_PhonebookList.Items.Clear();
            UsersPhone = FileOperatin.Read("../../Data/PhoneBook.csv");
            int count = 1;
            foreach (var item in UsersPhone)
            {
                if (item.ID == LU.user.Id)
                {
                    lstvw_PhonebookList.Items.Add(new ListViewItem(new string[] { count.ToString(), item.PersonName, item.PersonSurname, item.PhoneNumber.ToString(), item.PersonAddress, item.PersonDescription, item.Email }));
                    count++;
                }
            }
        }
        bool isValidEmail(string email) 
        {
            string emailTMP = email;
            Regex regexMail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regexMail.Match(emailTMP);
            if (match.Success)
                return true;
            else
                return false;
        }
        private void btn_AddPhone_Click(object sender, EventArgs e)
        {
            if (lstvw_PhonebookList.SelectedItems.Count == 1)
            {
                MessageBox.Show("Listeye birşey eklerken lütfen listeden herhangi bir şey seçmeyiniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if ((txt_NameForPhonebook.Text != "") && (txt_SurnameForPhonebook.Text != "") && (txt_PhoneAddress.Text != "") && (txt_PhoneDescription.Text != "") && (txt_PhoneMail.Text != ""))
            {
                if (isValidEmail(txt_PhoneMail.Text) == true)
                {
                    if (txt_AddPhone.Text.Length == 15)
                    {//long PhoneNumberTMP = Convert.ToInt64(txt_AddPhone.Text);
                        UsersPhoneBook UsersPhoneDetails = new UsersPhoneBook(txt_NameForPhonebook.Text, txt_SurnameForPhonebook.Text, txt_AddPhone.Text, txt_PhoneAddress.Text, txt_PhoneDescription.Text, txt_PhoneMail.Text, LU.user.Name, LU.user.Id);

                        UsersPhone.Add(UsersPhoneDetails);

                        string data = txt_NameForPhonebook.Text + "," + txt_SurnameForPhonebook.Text + "," + txt_AddPhone.Text + "," + txt_PhoneAddress.Text + "," + txt_PhoneDescription.Text + "," + txt_PhoneMail.Text + "," + LU.user.Name + "," + LU.user.Id;
                        if (FileOperatin.Write(data, "../../Data/PhoneBook.csv", true))
                        {
                            lbl_StatusAddPhone.Text = "Ekleme başarılı";
                            lbl_StatusAddPhone.ForeColor = Color.Green;
                            PhoneBook_Load(sender, e);
                        }
                        else
                        {
                            lbl_StatusAddPhone.Text = "Ekleme başarısız";
                            lbl_StatusAddPhone.ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Numaraniz 10 Haneden Olusmalidir. ORNEK - 1234567891.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Mail örnekte ki gibi olmalıdır... \nÖrnek; örnek@gmail.com ya da örnek@hotmail.com", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Kayıt etmek istediğiniz kişinin herhangi bir bilgisi boş olamaz\nLütfen bir bilgi ekleyiniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_DeletePhone_Click(object sender, EventArgs e)
        {
            if (lstvw_PhonebookList.SelectedItems.Count == 1)
            {
                int count = 0;
                foreach (var item in UsersPhone)
                {//item.PhoneNumber == Convert.ToInt64(lstvw_PhonebookList.SelectedItems[0].SubItems[3].Text)
                    if ((item.ID == LU.user.Id) && (item.PhoneNumber == lstvw_PhonebookList.SelectedItems[0].SubItems[3].Text))
                    {
                        UsersPhone.RemoveAt(count);
                        break;
                    }
                    count++;
                }
                lstvw_PhonebookList.Items.Remove(lstvw_PhonebookList.SelectedItems[0]);
                DosyaDuzeltme("../../Data/PhoneBook.csv");
                txt_AddPhone.Text = "";
                txt_NameForPhonebook.Text = "";
                txt_SurnameForPhonebook.Text = "";
                txt_PhoneAddress.Text = "";
                txt_PhoneDescription.Text = "";
                txt_PhoneMail.Text = "";
                PhoneBook_Load(sender, e);
            }
            else 
            {
                MessageBox.Show("Silmek istediğiniz kişiyi lütfen listeden seçiniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }    
         }

        void DosyaDuzeltme(string Path)
        {
            string myPath = Path;
            System.IO.File.WriteAllText(myPath, string.Empty);

            StreamWriter sw = new StreamWriter(myPath, true);

            for (int i = 0; i < UsersPhone.Count(); i++)
            {
                sw.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", UsersPhone[i].PersonName, UsersPhone[i].PersonSurname , UsersPhone[i].PhoneNumber , UsersPhone[i].PersonAddress, UsersPhone[i].PersonDescription, UsersPhone[i].Email, UsersPhone[i].Name, UsersPhone[i].ID);
            }
            sw.Close();
        }

        private void btn_UpdatePhone_Click(object sender, EventArgs e)
        {
            if (lstvw_PhonebookList.SelectedItems.Count == 1)
            {
                if (txt_NameForPhonebook.Text != "" && txt_SurnameForPhonebook.Text != "")
                {
                    if (txt_AddPhone.Text.Length == 15)
                    {
                        if (isValidEmail(txt_PhoneMail.Text) == true)
                        {
                            foreach (var item in UsersPhone)
                            {
                                if ((item.ID == LU.user.Id) && (item.PhoneNumber == lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[3].Text))
                                { //Convert.ToInt64(txt_AddPhone.Text);
                                    item.PersonName = txt_NameForPhonebook.Text;
                                    item.PersonSurname = txt_SurnameForPhonebook.Text;
                                    item.PhoneNumber = txt_AddPhone.Text;
                                    item.PersonAddress = txt_PhoneAddress.Text;
                                    item.PersonDescription = txt_PhoneDescription.Text;
                                    item.Email = txt_PhoneMail.Text;
                                }
                            }
                            lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[1].Text = txt_NameForPhonebook.Text;
                            lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[2].Text = txt_SurnameForPhonebook.Text;
                            lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[3].Text = txt_AddPhone.Text;
                            lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[4].Text = txt_PhoneAddress.Text;
                            lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[5].Text = txt_PhoneDescription.Text;
                            lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[6].Text = txt_PhoneMail.Text;
                            DosyaDuzeltme("../../Data/PhoneBook.csv");
                        }
                        else
                        {
                            MessageBox.Show("Mail örnekte ki gibi olmalıdır... \nÖrnek; örnek@gmail.com ya da örnek@hotmail.com", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }   
                    }
                    else
                    {
                        MessageBox.Show("Numaraniz 10 Haneden Olusmalidir. ORNEK - 1234567891.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Güncellemek istediğiniz kişinin Adı ya da Soyadı Boş olamaz\nLütfen bir Ad veya Soyad Ekleyiniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Güncellemek istediğiniz kişiyi lütfen listeden seçiniz...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void lstvw_PhonebookList_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lstvw_PhonebookList.SelectedItems.Count == 0)
                return;

            txt_NameForPhonebook.Text = lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[1].Text;
            txt_SurnameForPhonebook.Text = lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[2].Text;
            txt_AddPhone.Text = lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[3].Text;
            txt_PhoneAddress.Text = lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[4].Text;
            txt_PhoneDescription.Text = lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[5].Text;
            txt_PhoneMail.Text = lstvw_PhonebookList.Items[lstvw_PhonebookList.FocusedItem.Index].SubItems[6].Text;
        }

        private void txt_AddPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);

            if (e.KeyChar == 8)
            {
                txt_AddPhone.Text = "";
            }

            if (char.IsDigit(e.KeyChar))     //Count the digits already in the text.
            {  
                if ((sender as TextBox).Text.Count(Char.IsDigit) >= 10)
                    e.Handled = true;
            }
        }
        private void PhoneBook_Click(object sender, EventArgs e)
        {
            txt_AddPhone.Text = "";
            txt_NameForPhonebook.Text = "";
            txt_SurnameForPhonebook.Text = "";
            txt_PhoneAddress.Text = "";
            txt_PhoneDescription.Text = "";
            txt_PhoneMail.Text = "";
        }
        private void txt_AddPhone_TextChanged(object sender, EventArgs e)
        { 
            string s = txt_AddPhone.Text;
            if (s.Length == 0) return;
            if (s.Length == 10)
            {
                double sAsD = double.Parse(s);
                txt_AddPhone.Text = string.Format("{0:(###) ### ## ##}", sAsD).ToString();
            }
            if (txt_AddPhone.Text.Length > 1)
                txt_AddPhone.SelectionStart = txt_AddPhone.Text.Length;

            txt_AddPhone.SelectionLength = 0;
            lbl_StatusAddPhone.Text = txt_AddPhone.Text.Length.ToString();
        }
        private void txt_AddPhone_Click(object sender, EventArgs e)
        {
            txt_AddPhone.Text = "";
        }
    }
}
