﻿namespace Nesne_Lab_Project
{
    partial class UsersNoteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstvw_Usersnote = new System.Windows.Forms.ListView();
            this.columnHeader0 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_AddNotes = new System.Windows.Forms.Button();
            this.btn_Updatenotes = new System.Windows.Forms.Button();
            this.btn_Deletenotes = new System.Windows.Forms.Button();
            this.txt_Subject = new System.Windows.Forms.TextBox();
            this.lbl_StatusNotes = new System.Windows.Forms.Label();
            this.txt_Notes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lstvw_Usersnote
            // 
            this.lstvw_Usersnote.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader0,
            this.columnHeader1});
            this.lstvw_Usersnote.FullRowSelect = true;
            this.lstvw_Usersnote.GridLines = true;
            this.lstvw_Usersnote.HideSelection = false;
            this.lstvw_Usersnote.Location = new System.Drawing.Point(12, 12);
            this.lstvw_Usersnote.Name = "lstvw_Usersnote";
            this.lstvw_Usersnote.Size = new System.Drawing.Size(486, 382);
            this.lstvw_Usersnote.TabIndex = 0;
            this.lstvw_Usersnote.UseCompatibleStateImageBehavior = false;
            this.lstvw_Usersnote.View = System.Windows.Forms.View.Details;
            this.lstvw_Usersnote.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstvw_Usersnote_ItemSelectionChanged);
            // 
            // columnHeader0
            // 
            this.columnHeader0.Text = "ID";
            this.columnHeader0.Width = 31;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Subject";
            this.columnHeader1.Width = 433;
            // 
            // btn_AddNotes
            // 
            this.btn_AddNotes.Location = new System.Drawing.Point(840, 414);
            this.btn_AddNotes.Name = "btn_AddNotes";
            this.btn_AddNotes.Size = new System.Drawing.Size(75, 23);
            this.btn_AddNotes.TabIndex = 1;
            this.btn_AddNotes.Text = "Add";
            this.btn_AddNotes.UseVisualStyleBackColor = true;
            this.btn_AddNotes.Click += new System.EventHandler(this.btn_AddNotes_Click);
            // 
            // btn_Updatenotes
            // 
            this.btn_Updatenotes.Location = new System.Drawing.Point(840, 443);
            this.btn_Updatenotes.Name = "btn_Updatenotes";
            this.btn_Updatenotes.Size = new System.Drawing.Size(75, 23);
            this.btn_Updatenotes.TabIndex = 2;
            this.btn_Updatenotes.Text = "Update";
            this.btn_Updatenotes.UseVisualStyleBackColor = true;
            this.btn_Updatenotes.Click += new System.EventHandler(this.btn_Updatenotes_Click);
            // 
            // btn_Deletenotes
            // 
            this.btn_Deletenotes.Location = new System.Drawing.Point(840, 472);
            this.btn_Deletenotes.Name = "btn_Deletenotes";
            this.btn_Deletenotes.Size = new System.Drawing.Size(75, 23);
            this.btn_Deletenotes.TabIndex = 3;
            this.btn_Deletenotes.Text = "Delete";
            this.btn_Deletenotes.UseVisualStyleBackColor = true;
            this.btn_Deletenotes.Click += new System.EventHandler(this.btn_Deletenotes_Click);
            // 
            // txt_Subject
            // 
            this.txt_Subject.Location = new System.Drawing.Point(669, 12);
            this.txt_Subject.Multiline = true;
            this.txt_Subject.Name = "txt_Subject";
            this.txt_Subject.Size = new System.Drawing.Size(246, 33);
            this.txt_Subject.TabIndex = 4;
            this.txt_Subject.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Notes_KeyPress);
            // 
            // lbl_StatusNotes
            // 
            this.lbl_StatusNotes.AutoSize = true;
            this.lbl_StatusNotes.Location = new System.Drawing.Point(501, 397);
            this.lbl_StatusNotes.Name = "lbl_StatusNotes";
            this.lbl_StatusNotes.Size = new System.Drawing.Size(0, 13);
            this.lbl_StatusNotes.TabIndex = 5;
            // 
            // txt_Notes
            // 
            this.txt_Notes.Location = new System.Drawing.Point(504, 49);
            this.txt_Notes.Multiline = true;
            this.txt_Notes.Name = "txt_Notes";
            this.txt_Notes.Size = new System.Drawing.Size(411, 345);
            this.txt_Notes.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(521, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Subject(Can be just one line)";
            // 
            // UsersNoteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 505);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_Notes);
            this.Controls.Add(this.lbl_StatusNotes);
            this.Controls.Add(this.txt_Subject);
            this.Controls.Add(this.btn_Deletenotes);
            this.Controls.Add(this.btn_Updatenotes);
            this.Controls.Add(this.btn_AddNotes);
            this.Controls.Add(this.lstvw_Usersnote);
            this.Name = "UsersNoteForm";
            this.Text = "UsersNoteForm";
            this.Load += new System.EventHandler(this.UsersNoteForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstvw_Usersnote;
        private System.Windows.Forms.Button btn_AddNotes;
        private System.Windows.Forms.Button btn_Updatenotes;
        private System.Windows.Forms.Button btn_Deletenotes;
        private System.Windows.Forms.ColumnHeader columnHeader0;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.TextBox txt_Subject;
        private System.Windows.Forms.Label lbl_StatusNotes;
        private System.Windows.Forms.TextBox txt_Notes;
        private System.Windows.Forms.Label label1;
    }
}