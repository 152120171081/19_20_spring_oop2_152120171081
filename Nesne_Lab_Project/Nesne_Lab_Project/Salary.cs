﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    public class Salary
    {
        private int indexofdeneyim;
        private int indexofyasanilanil;
        private int indexofustogrenim;
        private int indexofyabancidilbilgisi;
        private int indexofyoneticilik;
        private int indexofailedurumu;

        private double deneyim;
        private double yasanilanil;
        private double ustogrenim;
        private double yabancidilbilgisi;
        private double yoneticilik;
        private double ailedurumu;

        private bool checkevli;
        private bool check06;
        private bool check718;
        private bool check18ustu;

        private bool checkBelge;
        private bool checkDiploma;
        private bool checkExtraBelge;
        private int extraBelgeAdet;

        private int idNumber;
        private double maasBilgisi;

        public int IndexOfDeneyim { get => indexofdeneyim; set => indexofdeneyim = value; }
        public int IndexOfYasanilanil { get => indexofyasanilanil; set => indexofyasanilanil = value; }
        public int IndexOfUstOgrenim { get => indexofustogrenim; set => indexofustogrenim = value; }
        public int IndexOfYabanciDil { get => indexofyabancidilbilgisi; set => indexofyabancidilbilgisi = value; }
        public int IndexOfYoneticilik { get => indexofyoneticilik; set => indexofyoneticilik = value; }
        public int IndexOfAileDurumu { get => indexofailedurumu; set => indexofailedurumu = value; }
        public double Deneyim { get => deneyim; set => deneyim = value; }
        public double Yasanilanil { get => yasanilanil; set => yasanilanil = value; }
        public double UstOgrenim { get => ustogrenim; set => ustogrenim = value; }
        public double Yabancidilbilgisi { get => yabancidilbilgisi; set => yabancidilbilgisi = value; }
        public double Yoneticilik { get => yoneticilik; set => yoneticilik = value; }
        public double AileDurumu { get => ailedurumu; set => ailedurumu = value; }
        public bool Checkevli { get => checkevli; set => checkevli = value; }
        public bool Check06 { get => check06; set => check06 = value; }
        public bool Check718 { get => check718; set => check718 = value; }
        public bool Check18ustu { get => check18ustu; set => check18ustu = value; }
        public bool CheckBelge { get => checkBelge; set => checkBelge = value; }
        public bool CheckDiploma { get => checkDiploma; set => checkDiploma = value; }
        public bool CheckExtraBelge { get => checkExtraBelge; set => checkExtraBelge = value; }
        public int ExtraBelgeAdet { get => extraBelgeAdet; set => extraBelgeAdet = value; }
        public int IdNumber { get => idNumber; set => idNumber = value; }
        public double MaasBilgisi { get => maasBilgisi; set => maasBilgisi = value; }

        public int DisplayReport()
        {
            return indexofdeneyim + indexofyasanilanil + indexofustogrenim + indexofyabancidilbilgisi + indexofyoneticilik + indexofailedurumu;
        }
        public static double DisplayMaas(List<Salary> SalaryList, int numberOfID)
        {
            try
            {
                for(int i = 0; i < SalaryList.Count; i++)
                {
                    if (SalaryList[i].IdNumber == numberOfID)
                    {
                        return SalaryList[i].maasBilgisi;
                    }
                }
                return 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
