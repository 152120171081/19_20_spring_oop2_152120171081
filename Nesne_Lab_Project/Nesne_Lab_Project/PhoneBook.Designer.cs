﻿namespace Nesne_Lab_Project
{
    partial class PhoneBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstvw_PhonebookList = new System.Windows.Forms.ListView();
            this.columnIDForPhoneBook = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnNameForPhoneBook = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPhoneForPhoneBook = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colForPhone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Address = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txt_AddPhone = new System.Windows.Forms.TextBox();
            this.btn_AddPhone = new System.Windows.Forms.Button();
            this.lbl_StatusAddPhone = new System.Windows.Forms.Label();
            this.txt_NameForPhonebook = new System.Windows.Forms.TextBox();
            this.txt_SurnameForPhonebook = new System.Windows.Forms.TextBox();
            this.lbl_PhoneNum = new System.Windows.Forms.Label();
            this.lbl_PhoneName = new System.Windows.Forms.Label();
            this.lbl_PhoneSurname = new System.Windows.Forms.Label();
            this.btn_UpdatePhone = new System.Windows.Forms.Button();
            this.btn_DeletePhone = new System.Windows.Forms.Button();
            this.lbl_Phonebookaddress = new System.Windows.Forms.Label();
            this.lbl_phonebookdescription = new System.Windows.Forms.Label();
            this.lbl_phonebookmail = new System.Windows.Forms.Label();
            this.txt_PhoneAddress = new System.Windows.Forms.TextBox();
            this.txt_PhoneDescription = new System.Windows.Forms.TextBox();
            this.txt_PhoneMail = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lstvw_PhonebookList
            // 
            this.lstvw_PhonebookList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnIDForPhoneBook,
            this.columnNameForPhoneBook,
            this.columnPhoneForPhoneBook,
            this.colForPhone,
            this.Address,
            this.columnHeader1,
            this.columnHeader2});
            this.lstvw_PhonebookList.FullRowSelect = true;
            this.lstvw_PhonebookList.HideSelection = false;
            this.lstvw_PhonebookList.Location = new System.Drawing.Point(12, 12);
            this.lstvw_PhonebookList.MultiSelect = false;
            this.lstvw_PhonebookList.Name = "lstvw_PhonebookList";
            this.lstvw_PhonebookList.Size = new System.Drawing.Size(826, 336);
            this.lstvw_PhonebookList.TabIndex = 0;
            this.lstvw_PhonebookList.UseCompatibleStateImageBehavior = false;
            this.lstvw_PhonebookList.View = System.Windows.Forms.View.Details;
            this.lstvw_PhonebookList.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstvw_PhonebookList_ItemSelectionChanged);
            // 
            // columnIDForPhoneBook
            // 
            this.columnIDForPhoneBook.Text = "ID";
            this.columnIDForPhoneBook.Width = 49;
            // 
            // columnNameForPhoneBook
            // 
            this.columnNameForPhoneBook.Text = "Name";
            this.columnNameForPhoneBook.Width = 53;
            // 
            // columnPhoneForPhoneBook
            // 
            this.columnPhoneForPhoneBook.Text = "Surname";
            this.columnPhoneForPhoneBook.Width = 74;
            // 
            // colForPhone
            // 
            this.colForPhone.Text = "PhoneNumber";
            this.colForPhone.Width = 122;
            // 
            // Address
            // 
            this.Address.Text = "Address";
            this.Address.Width = 172;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Description";
            this.columnHeader1.Width = 184;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "E-mail";
            this.columnHeader2.Width = 156;
            // 
            // txt_AddPhone
            // 
            this.txt_AddPhone.Location = new System.Drawing.Point(81, 443);
            this.txt_AddPhone.Name = "txt_AddPhone";
            this.txt_AddPhone.Size = new System.Drawing.Size(183, 20);
            this.txt_AddPhone.TabIndex = 2;
            this.txt_AddPhone.Click += new System.EventHandler(this.txt_AddPhone_Click);
            this.txt_AddPhone.TextChanged += new System.EventHandler(this.txt_AddPhone_TextChanged);
            this.txt_AddPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_AddPhone_KeyPress);
            // 
            // btn_AddPhone
            // 
            this.btn_AddPhone.Location = new System.Drawing.Point(338, 460);
            this.btn_AddPhone.Name = "btn_AddPhone";
            this.btn_AddPhone.Size = new System.Drawing.Size(75, 23);
            this.btn_AddPhone.TabIndex = 6;
            this.btn_AddPhone.Text = "Add";
            this.btn_AddPhone.UseVisualStyleBackColor = true;
            this.btn_AddPhone.Click += new System.EventHandler(this.btn_AddPhone_Click);
            // 
            // lbl_StatusAddPhone
            // 
            this.lbl_StatusAddPhone.AutoSize = true;
            this.lbl_StatusAddPhone.Location = new System.Drawing.Point(723, 361);
            this.lbl_StatusAddPhone.Name = "lbl_StatusAddPhone";
            this.lbl_StatusAddPhone.Size = new System.Drawing.Size(0, 13);
            this.lbl_StatusAddPhone.TabIndex = 3;
            // 
            // txt_NameForPhonebook
            // 
            this.txt_NameForPhonebook.Location = new System.Drawing.Point(81, 391);
            this.txt_NameForPhonebook.Name = "txt_NameForPhonebook";
            this.txt_NameForPhonebook.Size = new System.Drawing.Size(183, 20);
            this.txt_NameForPhonebook.TabIndex = 0;
            // 
            // txt_SurnameForPhonebook
            // 
            this.txt_SurnameForPhonebook.Location = new System.Drawing.Point(81, 417);
            this.txt_SurnameForPhonebook.Name = "txt_SurnameForPhonebook";
            this.txt_SurnameForPhonebook.Size = new System.Drawing.Size(183, 20);
            this.txt_SurnameForPhonebook.TabIndex = 1;
            // 
            // lbl_PhoneNum
            // 
            this.lbl_PhoneNum.AutoSize = true;
            this.lbl_PhoneNum.Location = new System.Drawing.Point(16, 450);
            this.lbl_PhoneNum.Name = "lbl_PhoneNum";
            this.lbl_PhoneNum.Size = new System.Drawing.Size(63, 13);
            this.lbl_PhoneNum.TabIndex = 6;
            this.lbl_PhoneNum.Text = "Phone Num";
            // 
            // lbl_PhoneName
            // 
            this.lbl_PhoneName.AutoSize = true;
            this.lbl_PhoneName.Location = new System.Drawing.Point(16, 398);
            this.lbl_PhoneName.Name = "lbl_PhoneName";
            this.lbl_PhoneName.Size = new System.Drawing.Size(35, 13);
            this.lbl_PhoneName.TabIndex = 7;
            this.lbl_PhoneName.Text = "Name";
            // 
            // lbl_PhoneSurname
            // 
            this.lbl_PhoneSurname.AutoSize = true;
            this.lbl_PhoneSurname.Location = new System.Drawing.Point(16, 424);
            this.lbl_PhoneSurname.Name = "lbl_PhoneSurname";
            this.lbl_PhoneSurname.Size = new System.Drawing.Size(49, 13);
            this.lbl_PhoneSurname.TabIndex = 8;
            this.lbl_PhoneSurname.Text = "Surname";
            // 
            // btn_UpdatePhone
            // 
            this.btn_UpdatePhone.Location = new System.Drawing.Point(338, 489);
            this.btn_UpdatePhone.Name = "btn_UpdatePhone";
            this.btn_UpdatePhone.Size = new System.Drawing.Size(75, 23);
            this.btn_UpdatePhone.TabIndex = 7;
            this.btn_UpdatePhone.Text = "Update";
            this.btn_UpdatePhone.UseVisualStyleBackColor = true;
            this.btn_UpdatePhone.Click += new System.EventHandler(this.btn_UpdatePhone_Click);
            // 
            // btn_DeletePhone
            // 
            this.btn_DeletePhone.Location = new System.Drawing.Point(338, 518);
            this.btn_DeletePhone.Name = "btn_DeletePhone";
            this.btn_DeletePhone.Size = new System.Drawing.Size(75, 23);
            this.btn_DeletePhone.TabIndex = 8;
            this.btn_DeletePhone.Text = "Delete";
            this.btn_DeletePhone.UseVisualStyleBackColor = true;
            this.btn_DeletePhone.Click += new System.EventHandler(this.btn_DeletePhone_Click);
            // 
            // lbl_Phonebookaddress
            // 
            this.lbl_Phonebookaddress.AutoSize = true;
            this.lbl_Phonebookaddress.Location = new System.Drawing.Point(16, 476);
            this.lbl_Phonebookaddress.Name = "lbl_Phonebookaddress";
            this.lbl_Phonebookaddress.Size = new System.Drawing.Size(45, 13);
            this.lbl_Phonebookaddress.TabIndex = 11;
            this.lbl_Phonebookaddress.Text = "Address";
            // 
            // lbl_phonebookdescription
            // 
            this.lbl_phonebookdescription.AutoSize = true;
            this.lbl_phonebookdescription.Location = new System.Drawing.Point(16, 502);
            this.lbl_phonebookdescription.Name = "lbl_phonebookdescription";
            this.lbl_phonebookdescription.Size = new System.Drawing.Size(60, 13);
            this.lbl_phonebookdescription.TabIndex = 12;
            this.lbl_phonebookdescription.Text = "Description";
            // 
            // lbl_phonebookmail
            // 
            this.lbl_phonebookmail.AutoSize = true;
            this.lbl_phonebookmail.Location = new System.Drawing.Point(16, 528);
            this.lbl_phonebookmail.Name = "lbl_phonebookmail";
            this.lbl_phonebookmail.Size = new System.Drawing.Size(41, 13);
            this.lbl_phonebookmail.TabIndex = 13;
            this.lbl_phonebookmail.Text = "E - mail";
            // 
            // txt_PhoneAddress
            // 
            this.txt_PhoneAddress.Location = new System.Drawing.Point(81, 469);
            this.txt_PhoneAddress.Name = "txt_PhoneAddress";
            this.txt_PhoneAddress.Size = new System.Drawing.Size(183, 20);
            this.txt_PhoneAddress.TabIndex = 3;
            // 
            // txt_PhoneDescription
            // 
            this.txt_PhoneDescription.Location = new System.Drawing.Point(81, 495);
            this.txt_PhoneDescription.Name = "txt_PhoneDescription";
            this.txt_PhoneDescription.Size = new System.Drawing.Size(183, 20);
            this.txt_PhoneDescription.TabIndex = 4;
            // 
            // txt_PhoneMail
            // 
            this.txt_PhoneMail.Location = new System.Drawing.Point(81, 521);
            this.txt_PhoneMail.Name = "txt_PhoneMail";
            this.txt_PhoneMail.Size = new System.Drawing.Size(183, 20);
            this.txt_PhoneMail.TabIndex = 5;
            // 
            // PhoneBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 588);
            this.Controls.Add(this.txt_PhoneMail);
            this.Controls.Add(this.txt_PhoneDescription);
            this.Controls.Add(this.txt_PhoneAddress);
            this.Controls.Add(this.lbl_phonebookmail);
            this.Controls.Add(this.lbl_phonebookdescription);
            this.Controls.Add(this.lbl_Phonebookaddress);
            this.Controls.Add(this.btn_DeletePhone);
            this.Controls.Add(this.btn_UpdatePhone);
            this.Controls.Add(this.lbl_PhoneSurname);
            this.Controls.Add(this.lbl_PhoneName);
            this.Controls.Add(this.lbl_PhoneNum);
            this.Controls.Add(this.txt_SurnameForPhonebook);
            this.Controls.Add(this.txt_NameForPhonebook);
            this.Controls.Add(this.lbl_StatusAddPhone);
            this.Controls.Add(this.btn_AddPhone);
            this.Controls.Add(this.txt_AddPhone);
            this.Controls.Add(this.lstvw_PhonebookList);
            this.Name = "PhoneBook";
            this.Text = "PhoneBook";
            this.Load += new System.EventHandler(this.PhoneBook_Load);
            this.Click += new System.EventHandler(this.PhoneBook_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstvw_PhonebookList;
        private System.Windows.Forms.ColumnHeader columnIDForPhoneBook;
        private System.Windows.Forms.ColumnHeader columnNameForPhoneBook;
        private System.Windows.Forms.ColumnHeader columnPhoneForPhoneBook;
        private System.Windows.Forms.TextBox txt_AddPhone;
        private System.Windows.Forms.Button btn_AddPhone;
        private System.Windows.Forms.Label lbl_StatusAddPhone;
        private System.Windows.Forms.TextBox txt_NameForPhonebook;
        private System.Windows.Forms.TextBox txt_SurnameForPhonebook;
        private System.Windows.Forms.ColumnHeader colForPhone;
        private System.Windows.Forms.Label lbl_PhoneNum;
        private System.Windows.Forms.Label lbl_PhoneName;
        private System.Windows.Forms.Label lbl_PhoneSurname;
        private System.Windows.Forms.Button btn_UpdatePhone;
        private System.Windows.Forms.Button btn_DeletePhone;
        private System.Windows.Forms.Label lbl_Phonebookaddress;
        private System.Windows.Forms.Label lbl_phonebookdescription;
        private System.Windows.Forms.Label lbl_phonebookmail;
        private System.Windows.Forms.TextBox txt_PhoneAddress;
        private System.Windows.Forms.TextBox txt_PhoneDescription;
        private System.Windows.Forms.TextBox txt_PhoneMail;
        private System.Windows.Forms.ColumnHeader Address;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
    }
}