﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nesne_Lab_Project
{
    public partial class Sing_In : Form
    {
        public Sing_In()
        {
            InitializeComponent();
        }

        private void btn_insert_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_name.Text == "" && txt_password.Text == "")
                {
                    MessageBox.Show("Boş bırakmayınız.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (txt_password.Text.Count() < 6)
                {
                    MessageBox.Show("Şifreyi en az 6 karakter giriniz.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if(!txt_mail.Text.Contains("@"))
                {
                    MessageBox.Show("Geçerli bir mail girmelisiniz.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                foreach (var item in Login.userlist)
                {
                    if (item.Name.Equals(txt_name.Text.ToLower()))
                    {
                        MessageBox.Show("Kullanıcı adi zaten ekli.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                User newuser;
                string gender;
                if (rdibtn_erkek.Checked)
                    gender = "Erkek";
                else
                    gender = "Kadın";
                if (Login.userlist.Count > 0)
                    newuser = new User(Login.userlist[Login.userlist.Count - 1].Id + 1, txt_name.Text.ToLower(), Hash.ComputeSha256Hash(txt_password.Text), "User", gender,txt_mail.Text, false);
                else
                    newuser = new User(0, txt_name.Text.ToLower(), Hash.ComputeSha256Hash(txt_password.Text), "Admin", gender, txt_mail.Text, false);
                Login.userlist.Add(newuser);
                if (FileOperatin.Write(newuser.ToString(), "../../Data/users.csv", true))
                {
                    lbl_result.Text = "Kayıt başarılı";
                    lbl_result.BackColor = Color.Green;
                    txt_name.Text = txt_password.Text = "";
                }
                else
                {
                    lbl_result.Text = "Kayıt başarısız";
                    lbl_result.BackColor = Color.Red;
                    txt_name.Text = txt_password.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eror", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt_password_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btn_insert_Click(null, null);
            else if (e.KeyChar == 27)
                btn_exit_Click(null, null);
        }
    }
}
