﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nesne_Lab_Project
{
    public partial class Reminder : Form
    {
        public Reminder()
        {
            InitializeComponent();
        }

        private int selectedItemOrder = -1;
        LoginedUser LU = LoginedUser.UserSingleton();
        List<int> IDs = new List<int>();
        private void Reminder_Load(object sender, EventArgs e)
        {
            cmbx_type.DataSource = new string[] { "Meeting", "Task" };
            cmbx_listType.DataSource = new string[] { "Tümü", "Meeting", "Task" };
            rdbtn_true.Checked = true;
            dtp_date.CustomFormat = "M/d/yyyy HH:mm:ss";
        } 

        void clearForm()
        {
            txt_description.Text = txt_title.Text = "";
            rdbtn_true.Checked = true;
            dtp_date.Value = DateTime.Now;
        }

        private void lstvw_messages_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lstvw_messages.SelectedItems.Count == 0)
                return;
            cmbx_type.SelectedItem = lstvw_messages.SelectedItems[0].SubItems[1].Text;
            txt_title.Text = lstvw_messages.SelectedItems[0].SubItems[2].Text;
            txt_description.Text = lstvw_messages.SelectedItems[0].SubItems[3].Text;
            dtp_date.Value =Convert.ToDateTime(lstvw_messages.SelectedItems[0].SubItems[4].Text);
            if (lstvw_messages.SelectedItems[0].SubItems[6].Text.Equals("Aktif"))
            {
                rdbtn_true.Checked = true;
            }
            else
            {
                rdbtn_false.Checked = true;
            }
            selectedItemOrder = int.Parse(lstvw_messages.SelectedItems[0].SubItems[0].Text);
        }

        private void fillListView()
        {
            if (lstvw_messages.Items.Count>0)
            lstvw_messages.Items.Clear();
            IDs.Clear();
            int count = 1;
            foreach (KeyValuePair<Message, int> item in LU.ReminderMessages)
            {
                if (cmbx_listType.SelectedItem.Equals("Meeting") && item.Key.Type.Equals("Meeting"))
                    lstvw_messages.Items.Add(new ListViewItem(new string[] { (count++).ToString(), item.Key.Type, item.Key.Title, item.Key.Description, item.Key.Time.ToString(), item.Value.ToString(), item.Key.Status ? "Aktif" : "Pasif" }));
                else if (cmbx_listType.SelectedItem.Equals("Task") && item.Key.Type.Equals("Task"))
                    lstvw_messages.Items.Add(new ListViewItem(new string[] { (count++).ToString(), item.Key.Type, item.Key.Title, item.Key.Description, item.Key.Time.ToString(), item.Value.ToString(), item.Key.Status ? "Aktif" : "Pasif" }));
                else if (cmbx_listType.SelectedItem.Equals("Tümü"))
                    lstvw_messages.Items.Add(new ListViewItem(new string[] { (count++).ToString(), item.Key.Type, item.Key.Title, item.Key.Description, item.Key.Time.ToString(), item.Value.ToString(), item.Key.Status ? "Aktif" : "Pasif" }));
                IDs.Add(item.Key.Id);
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if(txt_description.Text==""||txt_title.Text=="")
            {
                MessageBox.Show("Lütfen boş bırakmayınız.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            Message newMessage = new Message();
            if (Anaform.messages.Count > 0)
                newMessage.Id = int.Parse((Anaform.messages[Anaform.messages.Count - 1].Split(','))[0]) + 1;
            else
                newMessage.Id = 0;
            newMessage.UserId = LU.user.Id;
            newMessage.Type = cmbx_type.SelectedValue.ToString();
            newMessage.Title = txt_title.Text;
            newMessage.Description = txt_description.Text;
            newMessage.Time = dtp_date.Value;
            newMessage.Status = rdbtn_true.Checked;
            if (LU.MessageAdd(newMessage,Anaform.messages.Count))
            {
                fillListView();
                Anaform.messages.Add(newMessage.ToString()+",Kullanımda");
                FileOperatin.Write(newMessage.ToString() + ",Kullanımda", "../../Data/Reminder.csv", true);
            }
            else
            {
                MessageBox.Show("Eklenemedi. Aynı zamana ait 1den fazla mesaj tanımlanamaz!!", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            clearForm();
        }

        private void cmbx_listType_SelectedValueChanged(object sender, EventArgs e)
        {
            fillListView();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if(lstvw_messages.SelectedItems.Count==0)
            {
                MessageBox.Show("Lütfen bir satırı seçiniz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            Message tmp = new Message();
            tmp.Id = IDs[selectedItemOrder - 1];
            tmp.Time= Convert.ToDateTime(lstvw_messages.SelectedItems[0].SubItems[4].Text);
            if (LU.MessageDel(tmp))
            {
                fillListView();
                MessageBox.Show("Veri silindi.", "Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FileOperatin.Write(Anaform.messages, "../../Data/Reminder.csv", false);
            }
            else
                MessageBox.Show("Veri silinemedi.", "Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Information);
            clearForm();
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            if (lstvw_messages.SelectedItems.Count == 0)
            {
                MessageBox.Show("Lütfen bir satırı seçiniz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            Message tmp = new Message();
            tmp.Id = IDs[selectedItemOrder - 1];
            tmp.Time = Convert.ToDateTime(lstvw_messages.SelectedItems[0].SubItems[4].Text);
            Message newMessage = new Message();
            if (Anaform.messages.Count > 0)
                newMessage.Id = int.Parse((Anaform.messages[Anaform.messages.Count - 1].Split(','))[0]) + 1;
            else
                newMessage.Id = 0;
            newMessage.UserId = LU.user.Id;
            newMessage.Type = cmbx_type.SelectedValue.ToString();
            newMessage.Title = txt_title.Text;
            newMessage.Description = txt_description.Text;
            newMessage.Time = dtp_date.Value;
            newMessage.Status = rdbtn_true.Checked;
            if (LU.MessageUpdate(tmp,newMessage))
            {
                fillListView();
                Anaform.messages.Add(newMessage.ToString()+",Kullanımda");
                MessageBox.Show("Veri güncellendi.", "Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FileOperatin.Write(Anaform.messages, "../../Data/Reminder.csv", false);
            }
            else
                MessageBox.Show("Veri güncellenemedi.", "Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Information);

            clearForm();
        }
    }
}
