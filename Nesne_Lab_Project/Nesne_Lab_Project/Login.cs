﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nesne_Lab_Project
{
    public partial class Login : Form
    {
        public static List<User> userlist = new List<User>();
        public Login()
        {
            InitializeComponent();
            FileOperatin.Read("../../Data/users.csv", userlist);
            // logout check for timer to cancel close this one after logout 3 sec later
            if (!LogOUT)
                CheckRemember();
        }
        private LoginedUser LU;
        public static bool LogOUT=false;
        public static bool exit = false;

        private void btn_join_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_name.Text == "" || txt_pass.Text == "")
                {
                    MessageBox.Show("Şifre veya kullanıcı adını boş bırakamazsınız!!!", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (txt_pass.Text.Length < 6)
                {
                    MessageBox.Show("Şifre 6 basamaktan küçük olamaz!!!", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                bool status = false;
                foreach (var item in userlist)
                {

                    if (item.Name.ToLower().Equals(txt_name.Text.ToLower()))
                    {
                        if (item.Password.Equals(Hash.ComputeSha256Hash(txt_pass.Text)))
                        {
                            LoginUser(item);
                            status = true;
                            exit = false;
                            break;
                        }
                        else
                        {
                            lbl_result.Text = "Giriş başarısız.";
                            lbl_result.BackColor = Color.IndianRed;
                        }
                    }
                }
                if(!status)
                    MessageBox.Show("Kullanıcı kayıtlı değil yada bilgiler yanlış.", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eror", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tmr_time_Tick(object sender, EventArgs e)
        {
            try
            {
                tmr_time.Stop();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eror", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            exit=true;
            this.Close();
        }

        private void txt_name_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btn_join_Click(null, null);
            else if (e.KeyChar == 27)
                btn_exit_Click(null, null);
        }

        private void Login_Load(object sender, EventArgs e)
        {
            try
            {
                if (LogOUT)
                {
                    exit = true;
                    chbx_rememberme.Checked = false ;
                    
                    lbl_result.BackColor = SystemColors.Control;
                    foreach (var item in userlist)
                    {
                        if (item.Remember)
                        {
                            item.Remember = false;
                            FileOperatin.Write(userlist, "../../Data/users.csv", false);
                        }
                    }                    
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eror", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CheckRemember()
        {
            foreach (var item in userlist)
            {
                if(item.Remember)
                {
                    txt_name.Text = item.Name;
                    txt_pass.Text = "*******";
                    chbx_rememberme.Checked = true;
                    LoginUser(item);
                    break;
                }
            }
        }

        private void LoginUser(User u)
        {
            LU = LoginedUser.UserSingleton(u);           
            lbl_result.Text = "Giriş başarılı.";
            lbl_result.BackColor = Color.LightGreen;
            tmr_time.Start();
            if (chbx_rememberme.Checked && !(u.Remember))
            {
                u.Remember = true;
                FileOperatin.Write(userlist, "../../Data/users.csv", false);
            }
        }

        private void btn_singın_Click(object sender, EventArgs e)
        {
            try
            {
                Sing_In sI = new Sing_In();
                this.Hide();
                sI.ShowDialog();
                this.Show();
                Login_Load(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eror", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
