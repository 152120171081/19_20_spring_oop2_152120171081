﻿namespace Nesne_Lab_Project
{
    partial class SalaryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Deneyim = new System.Windows.Forms.Label();
            this.lbl_Yasanilanil = new System.Windows.Forms.Label();
            this.lbl_yabancidil = new System.Windows.Forms.Label();
            this.lbl_ustogrenim = new System.Windows.Forms.Label();
            this.lbl_ailedurumu = new System.Windows.Forms.Label();
            this.lbl_yoneticilik = new System.Windows.Forms.Label();
            this.cmbx_deneyim = new System.Windows.Forms.ComboBox();
            this.cmbx_yasanilanil = new System.Windows.Forms.ComboBox();
            this.cmbx_ustogrenim = new System.Windows.Forms.ComboBox();
            this.cmbx_yabancidil = new System.Windows.Forms.ComboBox();
            this.cmbx_yoneticilik = new System.Windows.Forms.ComboBox();
            this.cmbx_ailedurumu = new System.Windows.Forms.ComboBox();
            this.grpbx_Ailedurumu = new System.Windows.Forms.GroupBox();
            this.checkBox_18ustucocuk = new System.Windows.Forms.CheckBox();
            this.checkBox_7_18arasicocuk = new System.Windows.Forms.CheckBox();
            this.checkBox_06arasicocuk = new System.Windows.Forms.CheckBox();
            this.checkBox_evli_esicalismiyor = new System.Windows.Forms.CheckBox();
            this.groupBox_yabancidilbilgisi = new System.Windows.Forms.GroupBox();
            this.groupBox_extraBelge = new System.Windows.Forms.GroupBox();
            this.textBox_belgeAdetBilgisi = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_extraBelge = new System.Windows.Forms.CheckBox();
            this.checkBox_MezuniyetBelge = new System.Windows.Forms.CheckBox();
            this.checkBox_belgelendirilmisBelge = new System.Windows.Forms.CheckBox();
            this.button_Kaydet = new System.Windows.Forms.Button();
            this.button_Guncelle = new System.Windows.Forms.Button();
            this.grpbx_Ailedurumu.SuspendLayout();
            this.groupBox_yabancidilbilgisi.SuspendLayout();
            this.groupBox_extraBelge.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_Deneyim
            // 
            this.lbl_Deneyim.AutoSize = true;
            this.lbl_Deneyim.Location = new System.Drawing.Point(34, 31);
            this.lbl_Deneyim.Name = "lbl_Deneyim";
            this.lbl_Deneyim.Size = new System.Drawing.Size(48, 13);
            this.lbl_Deneyim.TabIndex = 0;
            this.lbl_Deneyim.Text = "Deneyim";
            // 
            // lbl_Yasanilanil
            // 
            this.lbl_Yasanilanil.AutoSize = true;
            this.lbl_Yasanilanil.Location = new System.Drawing.Point(34, 66);
            this.lbl_Yasanilanil.Name = "lbl_Yasanilanil";
            this.lbl_Yasanilanil.Size = new System.Drawing.Size(61, 13);
            this.lbl_Yasanilanil.TabIndex = 1;
            this.lbl_Yasanilanil.Text = "Yaşanılan İl";
            // 
            // lbl_yabancidil
            // 
            this.lbl_yabancidil.AutoSize = true;
            this.lbl_yabancidil.Location = new System.Drawing.Point(34, 140);
            this.lbl_yabancidil.Name = "lbl_yabancidil";
            this.lbl_yabancidil.Size = new System.Drawing.Size(90, 13);
            this.lbl_yabancidil.TabIndex = 3;
            this.lbl_yabancidil.Text = "Yabancı Dil Bilgisi";
            // 
            // lbl_ustogrenim
            // 
            this.lbl_ustogrenim.AutoSize = true;
            this.lbl_ustogrenim.Location = new System.Drawing.Point(34, 105);
            this.lbl_ustogrenim.Name = "lbl_ustogrenim";
            this.lbl_ustogrenim.Size = new System.Drawing.Size(65, 13);
            this.lbl_ustogrenim.TabIndex = 2;
            this.lbl_ustogrenim.Text = "Üst Öğrenim";
            // 
            // lbl_ailedurumu
            // 
            this.lbl_ailedurumu.AutoSize = true;
            this.lbl_ailedurumu.Location = new System.Drawing.Point(34, 215);
            this.lbl_ailedurumu.Name = "lbl_ailedurumu";
            this.lbl_ailedurumu.Size = new System.Drawing.Size(64, 13);
            this.lbl_ailedurumu.TabIndex = 5;
            this.lbl_ailedurumu.Text = "Aile Durumu";
            // 
            // lbl_yoneticilik
            // 
            this.lbl_yoneticilik.AutoSize = true;
            this.lbl_yoneticilik.Location = new System.Drawing.Point(34, 180);
            this.lbl_yoneticilik.Name = "lbl_yoneticilik";
            this.lbl_yoneticilik.Size = new System.Drawing.Size(89, 13);
            this.lbl_yoneticilik.TabIndex = 4;
            this.lbl_yoneticilik.Text = "Yöneticilik Görevi";
            // 
            // cmbx_deneyim
            // 
            this.cmbx_deneyim.FormattingEnabled = true;
            this.cmbx_deneyim.Items.AddRange(new object[] {
            "Hiçbiri",
            "2-4",
            "5-9",
            "10-14",
            "15-20",
            "20 yıl üstü"});
            this.cmbx_deneyim.Location = new System.Drawing.Point(201, 23);
            this.cmbx_deneyim.Name = "cmbx_deneyim";
            this.cmbx_deneyim.Size = new System.Drawing.Size(298, 21);
            this.cmbx_deneyim.TabIndex = 6;
            // 
            // cmbx_yasanilanil
            // 
            this.cmbx_yasanilanil.FormattingEnabled = true;
            this.cmbx_yasanilanil.Items.AddRange(new object[] {
            "TR10: İstanbul",
            "TR51: Ankara",
            "TR31: İzmir",
            "TR42: Kocaeli, Sakarya, Düzce, Bolu, Yalova",
            "TR21: Edirne, Kırklareli, Tekirdağ",
            "TR90: Trabzon, Ordu, Giresun, Rize, Artvin, Gümüşhane",
            "TR41: Bursa, Eskişehir, Bilecik",
            "TR32: Aydın, Denizli, Muğla",
            "TR62: Adana, Mersin",
            "TR22: Balıkesir, Çanakkale",
            "TR61: Antalya, Isparta, Burdur",
            "Diğer İller -"});
            this.cmbx_yasanilanil.Location = new System.Drawing.Point(201, 58);
            this.cmbx_yasanilanil.Name = "cmbx_yasanilanil";
            this.cmbx_yasanilanil.Size = new System.Drawing.Size(298, 21);
            this.cmbx_yasanilanil.TabIndex = 7;
            // 
            // cmbx_ustogrenim
            // 
            this.cmbx_ustogrenim.FormattingEnabled = true;
            this.cmbx_ustogrenim.Items.AddRange(new object[] {
            "Hiçbiri",
            "Meslek alanı ile ilgili yüksek lisans",
            "Meslek alanı ile ilgili doktora",
            "Meslek alanı ile ilgili doçentlik",
            "Meslek alanı ile ilgili olmayan yüksek lisans",
            "Meslek alanı ile ilgili olmayan doktora/doçentlik"});
            this.cmbx_ustogrenim.Location = new System.Drawing.Point(201, 97);
            this.cmbx_ustogrenim.Name = "cmbx_ustogrenim";
            this.cmbx_ustogrenim.Size = new System.Drawing.Size(298, 21);
            this.cmbx_ustogrenim.TabIndex = 8;
            // 
            // cmbx_yabancidil
            // 
            this.cmbx_yabancidil.FormattingEnabled = true;
            this.cmbx_yabancidil.Items.AddRange(new object[] {
            "Hiçbiri",
            "Belgelendirilmiş İngilizce bilgisi",
            "İngilizce eğitim veren okul mezuniyeti",
            "Belgelendirilmiş diğer yabancı dil bilgisi (her dil için)",
            "Birden fazla belgem var"});
            this.cmbx_yabancidil.Location = new System.Drawing.Point(201, 132);
            this.cmbx_yabancidil.Name = "cmbx_yabancidil";
            this.cmbx_yabancidil.Size = new System.Drawing.Size(298, 21);
            this.cmbx_yabancidil.TabIndex = 9;
            this.cmbx_yabancidil.SelectedIndexChanged += new System.EventHandler(this.cmbx_yabancidil_SelectedIndexChanged);
            // 
            // cmbx_yoneticilik
            // 
            this.cmbx_yoneticilik.FormattingEnabled = true;
            this.cmbx_yoneticilik.Items.AddRange(new object[] {
            "Hiçbiri",
            "Takım Lideri/Grup Yöneticisi/Teknik Yönetici/Yazılım Mimarı",
            "Proje Yöneticisi",
            "Direktör/Projeler Yöneticisi",
            "CTO/Genel Müdür",
            "Bilgi İşlem Sorumlusu/Müdürü (Bilgi İşlem biriminde en çok 5 bilişim personeli va" +
                "rsa)",
            "Bilgi İşlem Sorumlusu/Müdürü (Bilgi İşlem biriminde 5\'ten çok bilişim personeli v" +
                "arsa)"});
            this.cmbx_yoneticilik.Location = new System.Drawing.Point(201, 172);
            this.cmbx_yoneticilik.Name = "cmbx_yoneticilik";
            this.cmbx_yoneticilik.Size = new System.Drawing.Size(298, 21);
            this.cmbx_yoneticilik.TabIndex = 10;
            // 
            // cmbx_ailedurumu
            // 
            this.cmbx_ailedurumu.FormattingEnabled = true;
            this.cmbx_ailedurumu.Items.AddRange(new object[] {
            "Hiçbiri",
            "Evli ve eşi çalışmıyor",
            "0-6 yaş arası çocuk",
            "7-18 yaş arası çocuk",
            "18 yaş üstü çocuk (Üniversite lisans/ön lisans öğrencisi olmak koşuluyla) ",
            "Birden Fazla secenek icin tiklayiniz"});
            this.cmbx_ailedurumu.Location = new System.Drawing.Point(201, 207);
            this.cmbx_ailedurumu.Name = "cmbx_ailedurumu";
            this.cmbx_ailedurumu.Size = new System.Drawing.Size(298, 21);
            this.cmbx_ailedurumu.TabIndex = 11;
            this.cmbx_ailedurumu.SelectedIndexChanged += new System.EventHandler(this.cmbx_ailedurumu_SelectedIndexChanged);
            // 
            // grpbx_Ailedurumu
            // 
            this.grpbx_Ailedurumu.Controls.Add(this.checkBox_18ustucocuk);
            this.grpbx_Ailedurumu.Controls.Add(this.checkBox_7_18arasicocuk);
            this.grpbx_Ailedurumu.Controls.Add(this.checkBox_06arasicocuk);
            this.grpbx_Ailedurumu.Controls.Add(this.checkBox_evli_esicalismiyor);
            this.grpbx_Ailedurumu.Location = new System.Drawing.Point(505, 12);
            this.grpbx_Ailedurumu.Name = "grpbx_Ailedurumu";
            this.grpbx_Ailedurumu.Size = new System.Drawing.Size(405, 126);
            this.grpbx_Ailedurumu.TabIndex = 19;
            this.grpbx_Ailedurumu.TabStop = false;
            this.grpbx_Ailedurumu.Text = "Aile Durumu";
            // 
            // checkBox_18ustucocuk
            // 
            this.checkBox_18ustucocuk.AutoSize = true;
            this.checkBox_18ustucocuk.Location = new System.Drawing.Point(20, 100);
            this.checkBox_18ustucocuk.Name = "checkBox_18ustucocuk";
            this.checkBox_18ustucocuk.Size = new System.Drawing.Size(370, 17);
            this.checkBox_18ustucocuk.TabIndex = 24;
            this.checkBox_18ustucocuk.Text = "18 yaş üstü çocuk (Üniversite lisans/ön lisans öğrencisi olmak koşuluyla) ";
            this.checkBox_18ustucocuk.UseVisualStyleBackColor = true;
            // 
            // checkBox_7_18arasicocuk
            // 
            this.checkBox_7_18arasicocuk.AutoSize = true;
            this.checkBox_7_18arasicocuk.Location = new System.Drawing.Point(20, 77);
            this.checkBox_7_18arasicocuk.Name = "checkBox_7_18arasicocuk";
            this.checkBox_7_18arasicocuk.Size = new System.Drawing.Size(124, 17);
            this.checkBox_7_18arasicocuk.TabIndex = 23;
            this.checkBox_7_18arasicocuk.Text = "7-18 yaş arası çocuk";
            this.checkBox_7_18arasicocuk.UseVisualStyleBackColor = true;
            // 
            // checkBox_06arasicocuk
            // 
            this.checkBox_06arasicocuk.AutoSize = true;
            this.checkBox_06arasicocuk.Location = new System.Drawing.Point(20, 54);
            this.checkBox_06arasicocuk.Name = "checkBox_06arasicocuk";
            this.checkBox_06arasicocuk.Size = new System.Drawing.Size(118, 17);
            this.checkBox_06arasicocuk.TabIndex = 22;
            this.checkBox_06arasicocuk.Text = "0-6 yaş arası çocuk";
            this.checkBox_06arasicocuk.UseVisualStyleBackColor = true;
            // 
            // checkBox_evli_esicalismiyor
            // 
            this.checkBox_evli_esicalismiyor.AutoSize = true;
            this.checkBox_evli_esicalismiyor.Location = new System.Drawing.Point(20, 30);
            this.checkBox_evli_esicalismiyor.Name = "checkBox_evli_esicalismiyor";
            this.checkBox_evli_esicalismiyor.Size = new System.Drawing.Size(122, 17);
            this.checkBox_evli_esicalismiyor.TabIndex = 21;
            this.checkBox_evli_esicalismiyor.Text = "Evli ve eşi çalışmıyor";
            this.checkBox_evli_esicalismiyor.UseVisualStyleBackColor = true;
            // 
            // groupBox_yabancidilbilgisi
            // 
            this.groupBox_yabancidilbilgisi.Controls.Add(this.groupBox_extraBelge);
            this.groupBox_yabancidilbilgisi.Controls.Add(this.checkBox_extraBelge);
            this.groupBox_yabancidilbilgisi.Controls.Add(this.checkBox_MezuniyetBelge);
            this.groupBox_yabancidilbilgisi.Controls.Add(this.checkBox_belgelendirilmisBelge);
            this.groupBox_yabancidilbilgisi.Location = new System.Drawing.Point(505, 144);
            this.groupBox_yabancidilbilgisi.Name = "groupBox_yabancidilbilgisi";
            this.groupBox_yabancidilbilgisi.Size = new System.Drawing.Size(405, 161);
            this.groupBox_yabancidilbilgisi.TabIndex = 25;
            this.groupBox_yabancidilbilgisi.TabStop = false;
            this.groupBox_yabancidilbilgisi.Text = "Yabancı Dil Bilgisi";
            // 
            // groupBox_extraBelge
            // 
            this.groupBox_extraBelge.Controls.Add(this.textBox_belgeAdetBilgisi);
            this.groupBox_extraBelge.Controls.Add(this.label1);
            this.groupBox_extraBelge.Location = new System.Drawing.Point(20, 96);
            this.groupBox_extraBelge.Name = "groupBox_extraBelge";
            this.groupBox_extraBelge.Size = new System.Drawing.Size(379, 57);
            this.groupBox_extraBelge.TabIndex = 29;
            this.groupBox_extraBelge.TabStop = false;
            this.groupBox_extraBelge.Text = "Diğer Belgeler (İngilizce Hariç)";
            // 
            // textBox_belgeAdetBilgisi
            // 
            this.textBox_belgeAdetBilgisi.Location = new System.Drawing.Point(264, 19);
            this.textBox_belgeAdetBilgisi.Name = "textBox_belgeAdetBilgisi";
            this.textBox_belgeAdetBilgisi.Size = new System.Drawing.Size(100, 20);
            this.textBox_belgeAdetBilgisi.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Lütfen kutucuğa adet bilgisini giriniz...";
            // 
            // checkBox_extraBelge
            // 
            this.checkBox_extraBelge.AutoSize = true;
            this.checkBox_extraBelge.Location = new System.Drawing.Point(20, 73);
            this.checkBox_extraBelge.Name = "checkBox_extraBelge";
            this.checkBox_extraBelge.Size = new System.Drawing.Size(260, 17);
            this.checkBox_extraBelge.TabIndex = 31;
            this.checkBox_extraBelge.Text = "Belgelendirilmiş diğer yabancı dil bilgisi (her dil için)";
            this.checkBox_extraBelge.UseVisualStyleBackColor = true;
            this.checkBox_extraBelge.CheckedChanged += new System.EventHandler(this.checkBox_extraBelge_CheckedChanged);
            // 
            // checkBox_MezuniyetBelge
            // 
            this.checkBox_MezuniyetBelge.AutoSize = true;
            this.checkBox_MezuniyetBelge.Location = new System.Drawing.Point(20, 50);
            this.checkBox_MezuniyetBelge.Name = "checkBox_MezuniyetBelge";
            this.checkBox_MezuniyetBelge.Size = new System.Drawing.Size(199, 17);
            this.checkBox_MezuniyetBelge.TabIndex = 30;
            this.checkBox_MezuniyetBelge.Text = "İngilizce eğitim veren okul mezuniyeti";
            this.checkBox_MezuniyetBelge.UseVisualStyleBackColor = true;
            // 
            // checkBox_belgelendirilmisBelge
            // 
            this.checkBox_belgelendirilmisBelge.AutoSize = true;
            this.checkBox_belgelendirilmisBelge.Location = new System.Drawing.Point(20, 27);
            this.checkBox_belgelendirilmisBelge.Name = "checkBox_belgelendirilmisBelge";
            this.checkBox_belgelendirilmisBelge.Size = new System.Drawing.Size(169, 17);
            this.checkBox_belgelendirilmisBelge.TabIndex = 29;
            this.checkBox_belgelendirilmisBelge.Text = "Belgelendirilmiş İngilizce bilgisi ";
            this.checkBox_belgelendirilmisBelge.UseVisualStyleBackColor = true;
            // 
            // button_Kaydet
            // 
            this.button_Kaydet.Location = new System.Drawing.Point(424, 247);
            this.button_Kaydet.Name = "button_Kaydet";
            this.button_Kaydet.Size = new System.Drawing.Size(75, 23);
            this.button_Kaydet.TabIndex = 32;
            this.button_Kaydet.Text = "Kaydet";
            this.button_Kaydet.UseVisualStyleBackColor = true;
            this.button_Kaydet.Click += new System.EventHandler(this.button_Kaydet_Click);
            // 
            // button_Guncelle
            // 
            this.button_Guncelle.Location = new System.Drawing.Point(331, 247);
            this.button_Guncelle.Name = "button_Guncelle";
            this.button_Guncelle.Size = new System.Drawing.Size(75, 23);
            this.button_Guncelle.TabIndex = 33;
            this.button_Guncelle.Text = "Güncelle";
            this.button_Guncelle.UseVisualStyleBackColor = true;
            this.button_Guncelle.Click += new System.EventHandler(this.button_Guncelle_Click);
            // 
            // SalaryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(922, 553);
            this.Controls.Add(this.button_Guncelle);
            this.Controls.Add(this.button_Kaydet);
            this.Controls.Add(this.groupBox_yabancidilbilgisi);
            this.Controls.Add(this.grpbx_Ailedurumu);
            this.Controls.Add(this.cmbx_ailedurumu);
            this.Controls.Add(this.cmbx_yoneticilik);
            this.Controls.Add(this.cmbx_yabancidil);
            this.Controls.Add(this.cmbx_ustogrenim);
            this.Controls.Add(this.cmbx_yasanilanil);
            this.Controls.Add(this.cmbx_deneyim);
            this.Controls.Add(this.lbl_ailedurumu);
            this.Controls.Add(this.lbl_yoneticilik);
            this.Controls.Add(this.lbl_yabancidil);
            this.Controls.Add(this.lbl_ustogrenim);
            this.Controls.Add(this.lbl_Yasanilanil);
            this.Controls.Add(this.lbl_Deneyim);
            this.Name = "SalaryForm";
            this.Text = "SalaryForm";
            this.Load += new System.EventHandler(this.SalaryForm_Load);
            this.grpbx_Ailedurumu.ResumeLayout(false);
            this.grpbx_Ailedurumu.PerformLayout();
            this.groupBox_yabancidilbilgisi.ResumeLayout(false);
            this.groupBox_yabancidilbilgisi.PerformLayout();
            this.groupBox_extraBelge.ResumeLayout(false);
            this.groupBox_extraBelge.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Deneyim;
        private System.Windows.Forms.Label lbl_Yasanilanil;
        private System.Windows.Forms.Label lbl_yabancidil;
        private System.Windows.Forms.Label lbl_ustogrenim;
        private System.Windows.Forms.Label lbl_ailedurumu;
        private System.Windows.Forms.Label lbl_yoneticilik;
        private System.Windows.Forms.ComboBox cmbx_deneyim;
        private System.Windows.Forms.ComboBox cmbx_yasanilanil;
        private System.Windows.Forms.ComboBox cmbx_ustogrenim;
        private System.Windows.Forms.ComboBox cmbx_yabancidil;
        private System.Windows.Forms.ComboBox cmbx_yoneticilik;
        private System.Windows.Forms.ComboBox cmbx_ailedurumu;
        private System.Windows.Forms.GroupBox grpbx_Ailedurumu;
        private System.Windows.Forms.CheckBox checkBox_18ustucocuk;
        private System.Windows.Forms.CheckBox checkBox_7_18arasicocuk;
        private System.Windows.Forms.CheckBox checkBox_06arasicocuk;
        private System.Windows.Forms.CheckBox checkBox_evli_esicalismiyor;
        private System.Windows.Forms.GroupBox groupBox_yabancidilbilgisi;
        private System.Windows.Forms.TextBox textBox_belgeAdetBilgisi;
        private System.Windows.Forms.CheckBox checkBox_MezuniyetBelge;
        private System.Windows.Forms.CheckBox checkBox_belgelendirilmisBelge;
        private System.Windows.Forms.CheckBox checkBox_extraBelge;
        private System.Windows.Forms.GroupBox groupBox_extraBelge;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_Kaydet;
        private System.Windows.Forms.Button button_Guncelle;
    }
}