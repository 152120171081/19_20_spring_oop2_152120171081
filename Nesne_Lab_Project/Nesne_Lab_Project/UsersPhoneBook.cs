﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    public class UsersPhoneBook
    {
        private string name;
        private string phoneNumber;
        private string personName;
        private string personSurname;
        private string personAddress;
        private string personDescription;
        private string email;
        private int id;

        public UsersPhoneBook(string personName, string personSurname, string phoneNumber, string personAddress, string personDescription, string email, string name, int id)
        {  
            this.phoneNumber = phoneNumber;
            this.personName = personName;
            this.personSurname = personSurname;
            this.personAddress = personAddress;
            this.personDescription = personDescription;
            this.email = email;
            this.name = name;
            this.id = id;
        }

        public string PhoneNumber
        {
            get { return this.phoneNumber; }
            set { this.phoneNumber = value; }
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public string PersonName
        {
            get { return this.personName; }
            set { this.personName = value; }
        }

        public string PersonSurname
        {
            get { return this.personSurname; }
            set { this.personSurname = value; }
        }

        public string PersonAddress
        {
            get { return this.personAddress; }
            set { this.personAddress = value; }
        }

        public string PersonDescription
        {
            get { return this.personDescription; }
            set { this.personDescription = value; }
        }

        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }
    }
}
