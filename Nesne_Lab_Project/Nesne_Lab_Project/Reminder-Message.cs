﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nesne_Lab_Project
{
    public partial class Reminder_Message : Form
    {
        public Reminder_Message()
        {
            InitializeComponent();
        }
        public Message M;
        System.Media.SoundPlayer player = new System.Media.SoundPlayer();
        private void Reminder_Message_Load(object sender, EventArgs e)
        {
            this.Text=M.Title;
            lbl_tur.Text = M.Type;
            lbl_mesaj.Text = M.Description;
            lbl_time.Text = M.Time.ToString();
            tmr_sheke.Start();
            xOriginal= this.Location.X;
            yOriginal = this.Location.Y;
            player.SoundLocation = "../../Data/Sound/nudge.wav";
        }

        int xOriginal, yOriginal;
        bool move = false;
        int time = 0;
        private void tmr_sheke_Tick(object sender, EventArgs e)
        {
            if (time % 10 == 0)
                player.Play();
            time++;
            if (!move)
                this.Location = new Point(xOriginal - 15, yOriginal - 15);
            else
                this.Location = new Point(xOriginal + 15, yOriginal + 15);

            move = !move;
            System.Threading.Thread.Sleep(20);
            this.Location = new Point(xOriginal, yOriginal);
            if (time == 20)
                tmr_sheke.Stop();
        }
    }
}
