﻿namespace Nesne_Lab_Project
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lstvw_users = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbbtn_status = new System.Windows.Forms.ComboBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.txt_kadi = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_pass = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdibtn_kadin = new System.Windows.Forms.RadioButton();
            this.rdibtn_erkek = new System.Windows.Forms.RadioButton();
            this.ttp_pass = new System.Windows.Forms.ToolTip(this.components);
            this.txt_mail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.prgbr_sending = new System.Windows.Forms.ProgressBar();
            this.lbl_sending = new System.Windows.Forms.Label();
            this.tmr_sending = new System.Windows.Forms.Timer(this.components);
            this.lbl_result = new System.Windows.Forms.Label();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstvw_users
            // 
            this.lstvw_users.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.lstvw_users.FullRowSelect = true;
            this.lstvw_users.HideSelection = false;
            this.lstvw_users.Location = new System.Drawing.Point(12, 12);
            this.lstvw_users.MultiSelect = false;
            this.lstvw_users.Name = "lstvw_users";
            this.lstvw_users.Size = new System.Drawing.Size(508, 419);
            this.lstvw_users.TabIndex = 0;
            this.lstvw_users.UseCompatibleStateImageBehavior = false;
            this.lstvw_users.View = System.Windows.Forms.View.Details;
            this.lstvw_users.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstvw_users_ItemSelectionChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 0;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 71;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Password";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Gender";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Status";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "E-Mail";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(526, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Kullanici Adi:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(539, 179);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Status:";
            // 
            // cbbtn_status
            // 
            this.cbbtn_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbtn_status.FormattingEnabled = true;
            this.cbbtn_status.Location = new System.Drawing.Point(585, 176);
            this.cbbtn_status.Name = "cbbtn_status";
            this.cbbtn_status.Size = new System.Drawing.Size(155, 21);
            this.cbbtn_status.TabIndex = 7;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(576, 217);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 10;
            this.btn_save.Text = "Kaydet";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(657, 217);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 11;
            this.btn_exit.Text = "Çıkış";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // txt_kadi
            // 
            this.txt_kadi.Location = new System.Drawing.Point(599, 23);
            this.txt_kadi.Name = "txt_kadi";
            this.txt_kadi.Size = new System.Drawing.Size(141, 20);
            this.txt_kadi.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(553, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Parola:";
            // 
            // txt_pass
            // 
            this.txt_pass.Location = new System.Drawing.Point(599, 49);
            this.txt_pass.Name = "txt_pass";
            this.txt_pass.PasswordChar = '*';
            this.txt_pass.ReadOnly = true;
            this.txt_pass.Size = new System.Drawing.Size(141, 20);
            this.txt_pass.TabIndex = 12;
            this.txt_pass.Tag = "";
            this.txt_pass.Click += new System.EventHandler(this.txt_pass_Click);
            this.txt_pass.MouseHover += new System.EventHandler(this.txt_pass_MouseHover);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdibtn_kadin);
            this.groupBox1.Controls.Add(this.rdibtn_erkek);
            this.groupBox1.Location = new System.Drawing.Point(542, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 54);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cinsiyet";
            // 
            // rdibtn_kadin
            // 
            this.rdibtn_kadin.AutoSize = true;
            this.rdibtn_kadin.Location = new System.Drawing.Point(119, 19);
            this.rdibtn_kadin.Name = "rdibtn_kadin";
            this.rdibtn_kadin.Size = new System.Drawing.Size(52, 17);
            this.rdibtn_kadin.TabIndex = 0;
            this.rdibtn_kadin.TabStop = true;
            this.rdibtn_kadin.Text = "Kadın";
            this.rdibtn_kadin.UseVisualStyleBackColor = true;
            // 
            // rdibtn_erkek
            // 
            this.rdibtn_erkek.AutoSize = true;
            this.rdibtn_erkek.Location = new System.Drawing.Point(56, 19);
            this.rdibtn_erkek.Name = "rdibtn_erkek";
            this.rdibtn_erkek.Size = new System.Drawing.Size(53, 17);
            this.rdibtn_erkek.TabIndex = 0;
            this.rdibtn_erkek.TabStop = true;
            this.rdibtn_erkek.Text = "Erkek";
            this.rdibtn_erkek.UseVisualStyleBackColor = true;
            // 
            // ttp_pass
            // 
            this.ttp_pass.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.ttp_pass_Draw);
            // 
            // txt_mail
            // 
            this.txt_mail.Location = new System.Drawing.Point(598, 76);
            this.txt_mail.Name = "txt_mail";
            this.txt_mail.Size = new System.Drawing.Size(142, 20);
            this.txt_mail.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(553, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "E-Mail:";
            // 
            // prgbr_sending
            // 
            this.prgbr_sending.Location = new System.Drawing.Point(609, 256);
            this.prgbr_sending.Name = "prgbr_sending";
            this.prgbr_sending.Size = new System.Drawing.Size(131, 23);
            this.prgbr_sending.TabIndex = 16;
            this.prgbr_sending.Visible = false;
            // 
            // lbl_sending
            // 
            this.lbl_sending.AutoSize = true;
            this.lbl_sending.Location = new System.Drawing.Point(526, 261);
            this.lbl_sending.Name = "lbl_sending";
            this.lbl_sending.Size = new System.Drawing.Size(77, 13);
            this.lbl_sending.TabIndex = 17;
            this.lbl_sending.Text = "Şifre yollanıyor:";
            this.lbl_sending.Visible = false;
            // 
            // tmr_sending
            // 
            this.tmr_sending.Interval = 10;
            this.tmr_sending.Tick += new System.EventHandler(this.tmr_sending_Tick);
            // 
            // lbl_result
            // 
            this.lbl_result.AutoSize = true;
            this.lbl_result.Location = new System.Drawing.Point(573, 303);
            this.lbl_result.Name = "lbl_result";
            this.lbl_result.Size = new System.Drawing.Size(0, 13);
            this.lbl_result.TabIndex = 18;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Salary";
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 443);
            this.Controls.Add(this.lbl_result);
            this.Controls.Add(this.lbl_sending);
            this.Controls.Add(this.prgbr_sending);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_mail);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_pass);
            this.Controls.Add(this.txt_kadi);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.cbbtn_status);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstvw_users);
            this.Name = "Admin";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Admin_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstvw_users;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbbtn_status;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.TextBox txt_kadi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_pass;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdibtn_kadin;
        private System.Windows.Forms.RadioButton rdibtn_erkek;
        private System.Windows.Forms.ToolTip ttp_pass;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.TextBox txt_mail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar prgbr_sending;
        private System.Windows.Forms.Label lbl_sending;
        private System.Windows.Forms.Timer tmr_sending;
        private System.Windows.Forms.Label lbl_result;
        private System.Windows.Forms.ColumnHeader columnHeader7;
    }
}