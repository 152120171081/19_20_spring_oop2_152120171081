﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesne_Lab_Project
{
    class LoginedUser
    {
        public User user = null;

        private static LoginedUser _User;
        private SortedDictionary<Message, int> reminderMessages = new SortedDictionary<Message, int>(new MessageTimeComparer());

        private LoginedUser(User u) { this.user = u; }

        public static LoginedUser UserSingleton(User u = null)
        {
            if (_User == null)
                _User = new LoginedUser(u);
            else if (u != null)
                _User = new LoginedUser(u);
            return _User;
        }

        public bool MessageAdd(Message M,int order)
        {
            try
            {
                reminderMessages.Add(M, order);
                return true;
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return false;
            }
        }

        public bool MessageDel(Message M)
        {
            try
            {
                foreach (var item in reminderMessages)
                {
                    if (M.Id ==item.Key.Id)
                    {
                        int index = Anaform.messages.IndexOf(item.Key.ToString()+",Kullanımda");
                        string []temp=Anaform.messages[index].Split(',');
                        Anaform.messages[index] =temp[0] + ","+temp[1] + ","+temp[2] + ","+temp[3]+"," + temp[4] + "," + temp[5] + "," + temp[6] + "," + "Silindi";
                        reminderMessages.Remove(item.Key);
                        break;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool MessageUpdate(Message Mold,Message Mnew)
        {
            try
            {
                this.MessageDel(Mold);
                return this.MessageAdd(Mnew, Anaform.messages.Count);
            }
            catch
            {
                return false;
            }
        }
        public SortedDictionary<Message, int> ReminderMessages
        {
            get { return this.reminderMessages; }
        }
    }
}
