﻿namespace Nesne_Lab_Project
{
    partial class Anaform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Anaform));
            this.lbl_kullanici = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbtn_Cikis = new System.Windows.Forms.ToolStripButton();
            this.tsbtn_saveUsers = new System.Windows.Forms.ToolStripButton();
            this.tsbtn_Admin = new System.Windows.Forms.ToolStripButton();
            this.tsbtn_PhoneBook = new System.Windows.Forms.ToolStripButton();
            this.tsbtn_userInfo = new System.Windows.Forms.ToolStripButton();
            this.tsbtn_UsersNote = new System.Windows.Forms.ToolStripButton();
            this.tsbtn_Salary = new System.Windows.Forms.ToolStripButton();
            this.tsbtn_AddReminderNote = new System.Windows.Forms.ToolStripButton();
            this.tmr_ReminderShow = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_kullanici
            // 
            this.lbl_kullanici.AutoSize = true;
            this.lbl_kullanici.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_kullanici.Location = new System.Drawing.Point(12, 54);
            this.lbl_kullanici.Name = "lbl_kullanici";
            this.lbl_kullanici.Size = new System.Drawing.Size(0, 24);
            this.lbl_kullanici.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtn_Cikis,
            this.tsbtn_saveUsers,
            this.tsbtn_Admin,
            this.tsbtn_PhoneBook,
            this.tsbtn_userInfo,
            this.tsbtn_UsersNote,
            this.tsbtn_Salary,
            this.tsbtn_AddReminderNote});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbtn_Cikis
            // 
            this.tsbtn_Cikis.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbtn_Cikis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_Cikis.Name = "tsbtn_Cikis";
            this.tsbtn_Cikis.Size = new System.Drawing.Size(36, 22);
            this.tsbtn_Cikis.Text = "Çıkış";
            this.tsbtn_Cikis.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_Cikis.Click += new System.EventHandler(this.tsbtn_Cikis_Click);
            // 
            // tsbtn_saveUsers
            // 
            this.tsbtn_saveUsers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_saveUsers.Name = "tsbtn_saveUsers";
            this.tsbtn_saveUsers.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsbtn_saveUsers.RightToLeftAutoMirrorImage = true;
            this.tsbtn_saveUsers.Size = new System.Drawing.Size(111, 22);
            this.tsbtn_saveUsers.Text = "Kullanicilari Kaydet";
            this.tsbtn_saveUsers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_saveUsers.Click += new System.EventHandler(this.tsbtn_saveUsers_Click);
            // 
            // tsbtn_Admin
            // 
            this.tsbtn_Admin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_Admin.Name = "tsbtn_Admin";
            this.tsbtn_Admin.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsbtn_Admin.Size = new System.Drawing.Size(82, 22);
            this.tsbtn_Admin.Text = "Admin Paneli";
            this.tsbtn_Admin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_Admin.Click += new System.EventHandler(this.tsbtn_Admin_Click);
            // 
            // tsbtn_PhoneBook
            // 
            this.tsbtn_PhoneBook.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_PhoneBook.Name = "tsbtn_PhoneBook";
            this.tsbtn_PhoneBook.Size = new System.Drawing.Size(75, 22);
            this.tsbtn_PhoneBook.Text = "Phone Book";
            this.tsbtn_PhoneBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_PhoneBook.Click += new System.EventHandler(this.tsbtn_PhoneBook_Click);
            // 
            // tsbtn_userInfo
            // 
            this.tsbtn_userInfo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbtn_userInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_userInfo.Name = "tsbtn_userInfo";
            this.tsbtn_userInfo.Size = new System.Drawing.Size(91, 22);
            this.tsbtn_userInfo.Text = "Kullanıcı paneli";
            this.tsbtn_userInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_userInfo.Click += new System.EventHandler(this.tsbtn_userInfo_Click);
            // 
            // tsbtn_UsersNote
            // 
            this.tsbtn_UsersNote.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_UsersNote.Name = "tsbtn_UsersNote";
            this.tsbtn_UsersNote.Size = new System.Drawing.Size(68, 22);
            this.tsbtn_UsersNote.Text = "Users Note";
            this.tsbtn_UsersNote.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_UsersNote.Click += new System.EventHandler(this.tsbtn_UsersNote_Click);
            // 
            // tsbtn_Salary
            // 
            this.tsbtn_Salary.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtn_Salary.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_Salary.Name = "tsbtn_Salary";
            this.tsbtn_Salary.Size = new System.Drawing.Size(99, 22);
            this.tsbtn_Salary.Text = "Salary Calculator";
            this.tsbtn_Salary.Click += new System.EventHandler(this.tsbtn_Salary_Click);
            // 
            // tsbtn_AddReminderNote
            // 
            this.tsbtn_AddReminderNote.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtn_AddReminderNote.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_AddReminderNote.Image")));
            this.tsbtn_AddReminderNote.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_AddReminderNote.Name = "tsbtn_AddReminderNote";
            this.tsbtn_AddReminderNote.Size = new System.Drawing.Size(62, 22);
            this.tsbtn_AddReminderNote.Text = "Hatırlatıcı";
            this.tsbtn_AddReminderNote.Click += new System.EventHandler(this.tsbtn_AddReminderNote_Click);
            // 
            // tmr_ReminderShow
            // 
            this.tmr_ReminderShow.Interval = 1000;
            this.tmr_ReminderShow.Tick += new System.EventHandler(this.tmr_ReminderShow_Tick);
            // 
            // Anaform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lbl_kullanici);
            this.Name = "Anaform";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ana Form";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Anaform_FormClosing);
            this.Load += new System.EventHandler(this.Anaform_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_kullanici;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbtn_Cikis;
        private System.Windows.Forms.ToolStripButton tsbtn_saveUsers;
        private System.Windows.Forms.ToolStripButton tsbtn_Admin;
        private System.Windows.Forms.ToolStripButton tsbtn_PhoneBook;
        private System.Windows.Forms.ToolStripButton tsbtn_userInfo;
        private System.Windows.Forms.ToolStripButton tsbtn_UsersNote;
        private System.Windows.Forms.ToolStripButton tsbtn_Salary;
        private System.Windows.Forms.ToolStripButton tsbtn_AddReminderNote;
        private System.Windows.Forms.Timer tmr_ReminderShow;
    }
}

