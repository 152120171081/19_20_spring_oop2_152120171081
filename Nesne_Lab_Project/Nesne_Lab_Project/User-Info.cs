﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nesne_Lab_Project
{
    public partial class User_Info : Form
    {
        public User_Info()
        {
            InitializeComponent();
        }

        LoginedUser LU = LoginedUser.UserSingleton();
        List<string> Photo = new List<string>();
        Stack<DatasCareTaker> ctrlZ = new Stack<DatasCareTaker>();
        Stack<DatasCareTaker> ctrlY = new Stack<DatasCareTaker>();
        Datas Memento = new Datas();

        private void User_Info_Load(object sender, EventArgs e)
        {
            FileOperatin.Read("../../Data/Photo.csv", Photo);
            bool checkphoto = false;
            foreach (string item in Photo)
            {
                string[] tmp = item.Split(',');
                if(tmp[0].Equals(LU.user.Id.ToString()))
                {
                    pctbx_photo.Image = Base64.GetImage(tmp[1]);
                    checkphoto = true;
                }
            }
            if (!checkphoto)
                pctbx_photo.Image = imglst_userPhoto.Images[0];
            cmbox_gender.DataSource = new string[] { "Erkek", "Kadın" };
           
            cmbox_gender.SelectedItem = Memento.Gender = LU.user.Gender;
            Memento.Kadi = txt_kadi.Text = LU.user.Name;
            Memento.Mail = txt_mail.Text = LU.user.Mail;
            Memento.Photo = Base64.Tobase64Image(pctbx_photo.Image);
            txt_statu.Text = LU.user.Status;
            Memento.Pass = txt_pass.Text = "******";
            DatasCareTaker baseData = new DatasCareTaker();
            baseData.Memento = Memento.Backup();
            ctrlZ.Push(baseData);            
        }

        void GenderCheck()
        {
            if (LU.user.Gender.Equals("Erkek"))
            {
                cmbox_gender.SelectedItem = "Erkek";
                pctbx_photo.Image = imglst_userPhoto.Images[1];
            }
            else
            {
                cmbox_gender.SelectedItem = "Kadın";
                pctbx_photo.Image = imglst_userPhoto.Images[0];
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (btn_edit_Ckeck)
            {
                MessageBox.Show("Kayıt yapmadan önce önceki düzeltmeyi onaylayın...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (txt_kadi.Text == "")
            {
                MessageBox.Show("Kullanıcı adını boş bırakmayınız!!!", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!txt_mail.Text.Contains("@"))
            {
                MessageBox.Show("Geçerli mail adresi giriniz!!!", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (txt_pass.Text.Length < 6)
            {
                MessageBox.Show("Şifre 6 basamaktan küçük olamaz!!!", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (DialogResult.No == MessageBox.Show("Kaydetmek istediğinize emin misiniz?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Question))//Kaydetme kontrolü
                return;

            LU.user.Name = txt_kadi.Text;
            LU.user.Mail = txt_mail.Text;
            LU.user.Gender = cmbox_gender.SelectedValue.ToString();
            LU.user.Password = Hash.ComputeSha256Hash(txt_pass.Text);
            string imageBase64 = Base64.Tobase64Image(pctbx_photo.Image);

            bool changeCheck = false;
            foreach (var item in Login.userlist)
            {
                if (item.Id.Equals(LU.user.Id))
                {
                    if (!item.Gender.Equals(LU.user.Gender) || !item.Password.Equals(LU.user.Password) ||
                        !item.Name.Equals(LU.user.Name) || !item.Mail.Equals(LU.user.Mail))
                    {
                        cmbox_gender.SelectedItem = LU.user.Gender;
                        item.Name = LU.user.Name;
                        item.Mail = LU.user.Mail;
                        item.Gender = LU.user.Gender;
                        item.Password = LU.user.Password;
                        changeCheck = true;
                    }
                }
            }
            if (changeCheck)
                FileOperatin.Write(Login.userlist, "../../Data/users.csv", false);
            bool checkphoto = false;
            bool NotInList = true;
            for (int i = 0; i < Photo.Count; i++)
            {
                string[] tmp = Photo[i].Split(',');
                if (tmp[0].Equals(LU.user.Id.ToString()))
                {
                    NotInList = false;
                    if (!tmp[1].Equals(imageBase64))
                    {
                        Photo[i] = LU.user.Id.ToString() + "," + imageBase64;
                        checkphoto = true;
                    }
                }
            }
            if (NotInList)
                Photo.Add(LU.user.Id.ToString() + "," + imageBase64);
            if(checkphoto||NotInList)
                FileOperatin.Write(Photo, "../../Data/Photo.csv", false);
        }

        //Eğer değiştirmek istediyse
        bool btn_edit_Ckeck = false;
        private void btn_edit_Click(object sender, EventArgs e)
        {
            bool check = ((Button)sender).Text.Equals("Düzenle");
            if (check)
                ((Button)sender).Text = "Tamam";
            else
            {
                DatasCareTaker taker = new DatasCareTaker();
                taker.Memento = Memento.Backup();
                Memento.Kadi = txt_kadi.Text;
                Memento.Mail = txt_mail.Text;
                Memento.Pass = txt_pass.Text;
                Memento.Photo = Base64.Tobase64Image(pctbx_photo.Image);
                Memento.Gender = cmbox_gender.SelectedItem.ToString();
                //eğer yeni bir işlem olduysa ileri gitmeleri silmemiz gerek
                ctrlY.Clear();
                ctrlZ.Push(taker);
                ((Button)sender).Text = "Düzenle";
            }
            if (btn_edit_Ckeck && check)
            {
                ((Button)sender).Text = "Düzenle";
                MessageBox.Show("Önceki düzeltmeyi onaylayın...", "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            btn_edit_Ckeck = !btn_edit_Ckeck;
            switch (((Button)sender).Name)
            {
                case "btn_editKadi":
                    txt_kadi.ReadOnly = !txt_kadi.ReadOnly;
                    break;

                case "btn_editMail":
                    txt_mail.ReadOnly = !txt_mail.ReadOnly;
                    break;

                case "btn_editGender":
                    cmbox_gender.Enabled = !cmbox_gender.Enabled;
                    break;

                case "btn_editPass":
                    txt_pass.ReadOnly = !txt_pass.ReadOnly;
                    break;

                case "btn_editPhoto":
                    if (!btn_edit_Ckeck)
                        return;
                    MessageBox.Show("Max 150x150 min 130x135 boyutunda bir resim seçiniz. ","Uyarı",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "Image files | *.bmp; *.jpg; *.gif; *.png; *.tif | All files | *.*";
                    ofd.FilterIndex = 2;
                    ofd.RestoreDirectory = true;
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        if (imglst_userPhoto.Images.Count > 1)
                            imglst_userPhoto.Images.RemoveAt(1);
                        
                        imglst_userPhoto.Images.Add(Base64.GetImage(Base64.Tobase64Image(ofd.FileName)));
                        pctbx_photo.Image = imglst_userPhoto.Images[1];                        
                        btn_edit_Click(sender, e);
                    }
                    break;

                default:
                    return;
            }            
        }

        private void User_Info_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar==25)
            {
                //Ctrl+Y
                ctrlEvent(ctrlY,ctrlZ);
            }
            else if(e.KeyChar==26)
            {
                //Ctrl+Z
                ctrlEvent(ctrlZ, ctrlY);
            }
        }

        private void ctrlEvent(Stack<DatasCareTaker>source, Stack<DatasCareTaker> save)
        {
            if (source.Count > 0)
            {
                DatasCareTaker a = source.Pop();
                DatasCareTaker b = new DatasCareTaker();
                b.Memento = Memento.Backup();
                save.Push(b);
                Memento.ReturnBack(a.Memento);
                txt_kadi.Text = a.Memento.Kadi;
                txt_mail.Text = a.Memento.Mail;
                txt_pass.Text = a.Memento.Pass;
                cmbox_gender.SelectedItem = a.Memento.Gender;
                if (a.Memento.Photo == "")
                    return;
                if (imglst_userPhoto.Images.Count > 1)
                    imglst_userPhoto.Images.RemoveAt(1);
                imglst_userPhoto.Images.Add(Base64.GetImage(a.Memento.Photo));
                pctbx_photo.Image = imglst_userPhoto.Images[1];
            }
        }
    }
}
